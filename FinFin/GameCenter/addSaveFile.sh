cd minigame
for dir in */
do
    cd $dir
    if [ ! -e "save.txt" ]
    then
	touch "save.txt"
    fi
    cd ..
done
cd ..