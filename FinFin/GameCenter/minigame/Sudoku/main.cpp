/**
 * @brief main class for the sudoku game 
 *      left click to add one to the number, right click to minus one to the number
 *      you can not change number in red
 *      try to make the number unique for all row and colume, as well as all the 3X3 grids
 * @author Wenxuan
 * */


#include <iostream>
#include <GL/glut.h> 
#include <IL/il.h>
using namespace std;

//set the length and width of the game

static const  int sizeWindow=500;
//main map, 0 means no value, positive values mean values what we can move, negative means values which had been locked.
int map[9][9];
int mousex, mousey;                     //store mousex and mousey when mouse pressed
int win=0;
GLuint texid[19];                        //texture position, a question mark and 2*9 numbers
int numUnlock=20;                       //number which we can move


/**
 * @brief use to unlock a number so player can change the number
 *        when sudoku get init, every number is fulled, use this method to take out somenumber so play can play
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void unLock(){
    if(numUnlock>80){
        cout<<"this is too much, make it less than 81"<<endl;
    }
    for(int i=0;i<9;i++){
        for(int e=0;e<9;e++){
            map[i][e]=map[i][e]*-1;
        }
    }
    for(int i=0;i<numUnlock;i++){
        int x=rand()%9;
        int y=rand()%9;
        while(map[x][y]==0){
            x=rand()%9;
            y=rand()%9;
        }
        map[x][y]=0;
    }
    
}


/**
 * @brief check if player win everytimes the number change
 * @parameter void
 * @return bool: true for player win, false for player did not win
 * @author Wenxuan
 * */
bool checkWin(){
    
    
    //check for row and column
    for(int i=0;i<9;i++){
        int xPoss[9];
       
        int yPoss[9];
         for(int u=0;u<9;u++){
                xPoss[u]=u;
                yPoss[u]=u;
        }
        for(int e=0;e<9;e++){
           
            
            if(map[i][e]==0){
                return false;
            }
            if(xPoss[abs(map[i][e])-1]==-1||yPoss[abs(map[e][i])-1]==-1){//if appear a number already have, return false
                    return false;
            }
            xPoss[abs(map[i][e])-1]=-1;
            yPoss[abs(map[e][i])-1]=-1;
        }
    }
    
    
    //check for gride
    for(int i=0;i<3;i++){
        int poss[9];
       
        for(int e=0;e<3;e++){
            
             for(int u=0;u<9;u++){
                poss[u]=u;
                poss[u]=u;
        }
            
            for(int i2=0;i2<3;i2++){
                
                for(int e2=0;e2<3;e2++){
                    if(poss[abs(map[i*3+i2][e*3+e2])-1]==-1){
                        return false;
                    }
                    poss[abs(map[i*3+i2][e*3+e2])-1]=-1;
                    
                    
                 }   
            }
        }
    }
    
    
    return true;
}
/**
 * @brief called in the mouse listener, minus one to the number if unlock
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void doRightClick(){
   
    int x=9*mousex/sizeWindow;
    int y=9*(sizeWindow-mousey)/sizeWindow;
    if(map[x][y]>=0){
        map[x][y]--;
        if(map[x][y]<=0){
            map[x][y]=9;
        }
    }
    
    if(checkWin()){
        win=1;
    }
    checkWin();
    cout<<win<<endl;
    
}
/**
 * @brief called in the mouse listener, add one to the number if unlock
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void doLeftClick(){
   
    int x=9*mousex/sizeWindow;
    int y=9*(sizeWindow-mousey)/sizeWindow;
    if(map[x][y]>=0){
            map[x][y]++;
    }
    if(map[x][y]==10){
            map[x][y]=1;
    }
    if(checkWin()){
            win=1;
    }
    checkWin();
    cout<<win<<endl;
    
}


/**
 * @brief use to init the map use to play the game
 *        recurse through all position of the map to build a sudoku
 * @param int x: current x position to recurse
 *            int y: current y position to recurse
 * @return bool true for finish the map, false for unable to finish the map
 * @author Wenxuan
 * */
bool makeMap(int x, int y){
    
    //if already have filled all space, return it
    int get=1;
    for(int i=0;i<9;i++){
       for(int e=0;e<9;e++){
           if(map[i][e]==0){
                get=0;
            }
        } 
    }
    if(get==1){
            return true;
    }
    
    //get numbers not been used in the same row/column/grid
    int possable[10][2];
    for(int i=0;i<10;i++){
            possable[i][0]=i;
    }
    for(int i=0;i<9;i++){
        possable[map[x][i]][0]=-1;
        possable[map[i][y]][0]=-1;
    }
    int x2=x/3;
    int y2=y/3;
    for(int i=0;i<3;i++){
       for(int e=0;e<3;e++){
            possable[map[x2*3+i][y2*3+e]][0]=-1;
        } 
    }
    
    
    //random the number so it generate a random sudoku
    for(int i=0;i<=9;i++){
        possable[i][1]=rand()%100;
    }
    
    //bubble sort(is easy to make, and this is a small array, no need for complax sort)
    for(int x=0;x<9;x++){
        for(int y=0;y<9;y++){
            if(possable[y][1]>possable[y+1][1]){
                int temp;
                temp=possable[y][0];
                possable[y][0]=possable[y+1][0];
                possable[y+1][0]=temp;
                temp=possable[y][1];
                possable[y][1]=possable[y+1][1];
                possable[y+1][1]=temp;
            }
        }
    }
    
//use recursion to fill the next spot
    for(int i=0;i<10;i++){
        if(possable[i][0]>0){
            map[x][y]=possable[i][0];
            int newx=x+1;
            int newy=y;
            if(newx>8){
                    newx=0;
                    newy=y+1;
            }
            if(makeMap(newx, newy)){
                return true;
            }
            map[x][y]=0;
        }
       
    }
    
    return false;
    
}
    
/**
 * @brief use to display the game
 * @param void
 * @return void
 * @author Wenxuan
 * */
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, 0);
    //began the gl
    if(win!=1){
        //draw the numbers
        for(int x=0;x<9;x++){
            for(int y=0;y<9;y++){
                
                //set testure to no values
                int tex=0;
                
                //if number is locked, lock number texture start from 10, so make the start nine
                //and minus the values of the locked number to the texture id
                if(map[x][y]<0){
                   tex=9;
                   tex-=map[x][y];
                }else{
                    
                    //if is not locked, add the number to the id
                    tex+=map[x][y];
                    
                }
                
                //draw the numbers
                glBindTexture(GL_TEXTURE_2D, texid[tex]); 
                glBegin(GL_QUADS); 
                glTexCoord2i(x, y);glVertex2f(x,y);     
                glTexCoord2i(x+1, y);glVertex2f(x+1,y);   
                glTexCoord2i(x+1, y+1);glVertex2f(x+1, y+1); 
                glTexCoord2i(x, y+1);glVertex2f(x, y+1); 
                glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
                glEnd();
                
            }
        }
        //draw line for each grads
        glBegin(GL_QUADS);
        glColor3f(0.0f, 0.0f, 0.0f);
        for(int x=3;x<9;x+=3){
            for(int y=3;y<9;y+=3){
                 
                glVertex2f(x-0.02,0);     
                glVertex2f(x-0.02,9);   
                glVertex2f(x+0.02, 9); 
                glVertex2f(x+0.02, 0); 
                glVertex2f(0,x-0.02);     
                glVertex2f(9,x-0.02);   
                glVertex2f(9,x+0.02); 
                glVertex2f(0,x+0.02);  
            }
            
        }
        glColor3f(1.0f, 1.0f, 1.0f);
        glEnd();
        
    }else{
        for(int i=0;i<9;i++){
            for(int e=0;e<9;e++){
                if(map[i][e]>0){
                    map[i][e]=map[i][e]*-1;
                    win=2;
                }
              
            }  
        }  
        
    }
    glFlush();
    
    
}

/**
 * @brief the mouse listener, call mouseClick method when mouse click
 * @param int button which button of the mouse
 *            int state is it push in or release
 *            int x mouse x position
 *            int y mouse y position
 * @return void
 * @author Wenxuan
 * */
void myMouseFunc(int button, int state, int x, int y){ 
    if(win!=0){
        glutPostRedisplay(); 
            return;
    }
	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		mousex=x;
        mousey=y;
		doLeftClick();
        glutPostRedisplay(); 
	}
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		mousex=x;
        mousey=y;
		doRightClick();
        glutPostRedisplay(); 
	}
}
/**
 * @brief it load the image in the given filename using devil
 *      return -1 if error
 * @param char *filename filename to load from
 * @return int -1 if error
 * @author Wenxuan
 * */
int LoadImage(char *filename)
{
    ILboolean success; 
    ILuint image; 
    ilGenImages(1, &image); // Generation of one image name
    ilBindImage(image); //Binding of image name
    success = ilLoadImage(filename); // Loading of the image filename by DevIL
    if (success){
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); 
        if (!success){
            return -1;
        }
    }else{
        return -1;
    }
    return image;
}

/** 
 * @brief use to call LoadImage method, it will load the filename texture to the given index
 * @param string filename: the filename that this method will load, 
 *             int index: index of where will the texture be load to
 * @return void
 * @author Wenxuan
 * */
void autoLoad(string fileName, int index){
    
    /* load the file picture with DevIL */
    int  image = LoadImage(&fileName[0]);
    if ( image == -1 )
    {
        printf("Can't load picture file by DevIL ");
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
       glGenTextures(19, &texid[index]); /* Texture name generation */
       glBindTexture(GL_TEXTURE_2D, texid[index]); /* Binding of texture name */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
       glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 
        0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */
    
    
}

/** 
 * overwrite
 * @brief to make sure user will not reshape the window
 * @param GLsizei newwidth: the passin parameter, GLsizei newheight: the passin parameter
 *              both parameter are not used in here, since we don't want to resize the window.
 * @return void
 * @author Wenxuan Zhang
 * */
void reshape(GLsizei newwidth, GLsizei newheight) 
{  
    glutReshapeWindow(sizeWindow,sizeWindow);
}


/** 
 * @brief clear the board on init gl
 *      also set the vertex and viewport for gl
 * @param void
 * @return void
 * @author Wenxuan
 * */
void initGL(){
     glViewport(0, 0, sizeWindow,sizeWindow);              //use a screen size of WIDTH x HEIGHT
     glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
     gluOrtho2D(0,9,0,9);                                   //set the sceen coordinate from 0 to 9
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
    }



/** 
 * @brief use to init the setting the game and run the mainloop 
 * @param int, char **
 * @return int
 * @author Wenxuan
 * */
int main (int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    makeMap(0,0);
    unLock();
    //make the game window
    int argc1 = 1;
    char *argv1[1]={(char*)"Sudoku"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Games"); 
    //add displayfunction, mouse listener, and init the gl
    glutDisplayFunc(display); 
    glutMouseFunc(myMouseFunc);
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    initGL();
    //check if the user have the correct DevIL version
     if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
     {
           printf("wrong DevIL version ");
           return -1;
     }
     
    //init the DevIL, load the texture
    ilInit(); 
    autoLoad("questionMark.jpg",0);
    autoLoad("one.jpg",1);
    autoLoad("two.jpg",2);
    autoLoad("three.jpg",3);
    autoLoad("four.jpg",4);
    autoLoad("five.jpg",5);
    autoLoad("six.jpg",6);
    autoLoad("seven.jpg",7);
    autoLoad("eight.jpg",8);
    autoLoad("nine.jpg",9);
    autoLoad("onelock.jpg",10);
    autoLoad("twolock.jpg",11);
    autoLoad("threelock.jpg",12);
    autoLoad("fourlock.jpg",13);
    autoLoad("fivelock.jpg",14);
    autoLoad("sixlock.jpg",15);
    autoLoad("sevenlock.jpg",16);
    autoLoad("eightlock.jpg",17);
    autoLoad("ninelock.jpg",18);
    glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
     
    glutMainLoop();
}
