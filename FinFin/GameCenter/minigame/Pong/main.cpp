/** 
 * @file main.cpp
 * @brief Rhis is the main game file, the game is run here.
 * @author Bohr Deng
 */

#include "Ball.h"
#include "Paddle.h"
#include <GL/glut.h>  //glut is the openGL utility toolkit
#include <iostream>
#include <string>
#include <unistd.h> 
using namespace std;

Paddle p1 = Paddle(10, 175, 20, 5, 50);
Paddle p2 = Paddle(385, 175, 20, 5, 50);
Ball ball = Ball(195, 195, 1, 1, 5 ,5);

bool keystates[256];    //this is to keep track of multiple keys for multi-input
float ballXdir = 0, ballYdir = 0;  //Ball direction factors
float xMag = 1;     //Ball's x speed magnitude factor
int serving = 1;    //indicates which player is serving the ball
int oldTimeSinceStart = 0;      //this variable is used for passive rendering
int p1Score = 0, p2Score = 0;   //Scores for each player


/**
 * @name scale()
 * @brief This function scales the coordinates of the object to the opengl matrix.
 * Corners of the window/matrix:
 *  Top left (0,2)
 *  Top right (2,2)
 *  Bottom left (0, 0)
 *  Bottom right (2, 0)
 * To scale objects size to the openGL matrix, do 2*(value/400); the window is 2x2
 * @param val The size value to be scaled to the matrix
 */
float scale(float val){
    return 2*val/400;
}

/**
 * @name drawPaddle()
 * @brief This function draws the paddle of each player.
 * @param p The paddle of a player.
 */
void drawPaddle(Paddle p){
    float BLx = scale(p.get_xPos()), BLy = scale(p.get_yPos());   //bottom left corner coordintes
    float TLx = scale(p.get_xPos()), TLy = scale(p.get_yPos() + p.get_height());     //top left corner coordinates
    float BRx = scale(p.get_xPos()+p.get_width()), BRy = scale(p.get_yPos());    //bottom right corner coordinates
    float TRx = scale(p.get_xPos()+p.get_width()), TRy = scale(p.get_yPos()+p.get_height());    //top right corner coordinates
 
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1); //Set the colour to white
    glVertex2f(BLx, BLy);
    glVertex2f(TLx, TLy);
    glVertex2f(TRx, TRy);
    glVertex2f(BRx, BRy);
    glEnd();
}

/**
 * @name drawBall()
 * @brief This function draws the ball.
 */
void drawBall(){
    float BLx = scale(ball.get_xPos()), BLy = scale(ball.get_yPos());   //bottom left corner coordintes
    float TLx = scale(ball.get_xPos()), TLy = scale(ball.get_yPos() + ball.get_height());     //top left corner coordinates
    float BRx = scale(ball.get_xPos()+ball.get_width()), BRy = scale(ball.get_yPos());    //bottom right corner coordinates
    float TRx = scale(ball.get_xPos()+ball.get_width()), TRy = scale(ball.get_yPos()+ball.get_height());    //top right corner coordinates
 
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1); //Set the colour to white
    glVertex2f(BLx, BLy);
    glVertex2f(TLx, TLy);
    glVertex2f(TRx, TRy);
    glVertex2f(BRx, BRy);
    glEnd();
}

/**
 * @name drawScore()
 * @brief This function draws the score of each player.
 */
void drawScore(){
    string p1s = to_string(p1Score);
    string p2s = to_string(p2Score);
    glRasterPos2d(scale(100), scale(375));
    for (int i=0; i<p1s.length(); i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, p1s[i]);
    }
    glRasterPos2d(scale(300), scale(375));
    for (int i=0; i<p2s.length(); i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, p2s[i]);
    }

}

/**
 * @name drawWinner()
 * @brief This function draws the winner message when the game is exited.
 */
void drawWinner(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    string winner;
    glRasterPos2d(scale(110), scale(200));

    if (p1Score >p2Score){
        winner = "Player 1 is the winner!";
    } else if (p2Score > p1Score){
        winner = "Player 2 is the winner!";
    }
    else {
        glRasterPos2d(scale(190), scale(200));
        winner = "Tie!";
    }

    for (int i=0; i<winner.length(); i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, winner[i]);
    }

    glutSwapBuffers();      //push image to the screen
    sleep(3);    //wait for 3 sec before closing
    exit(0);
}

/**
 * @name draw()
 * @brief This function calls most of the draw functions and displays to the screen 
 */
void draw(){
    /* Corners of the window:
     * Top left (0,1)
     * Top right (1,1)
     * Bottom left (0, 0)
     * Bottom right (1, 0)
     * 
     * To scale objects pixel size to the openGL matrix, do 2*(value/400)
     */ 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);     //clears the screen

    drawPaddle(p1);
    drawPaddle(p2);
    drawBall();
    drawScore();
    
    glutSwapBuffers();      //push image to the screen
}


/**
 * @name activeUpdate()
 * @brief This updates events that are actively triggered, such as the paddles.
 * This method is supposed to update player's paddle when keys are pressed.
 * For player 1, 'w' and 's' is up and down.
 * For player 2, 'i' and 'k' is up and down.
 */
void activeUpdate(){
    // this is esc key for immediate quiting and displaying the player with higher score as winner
    if (keystates[27] == true)
		drawWinner();
    
    //player 1 controls and collision detection
    if (keystates[119] == true){
        // move player 1 paddle up
        if ( scale(p1.get_yPos()+p1.get_height()) >= 2){
            p1.set_yPos(400-p2.get_height());
        }
        else{
            p1.update(1);
        }
    }
    if (keystates[115] == true){
        // move player 1 paddle down
        if ( scale(p1.get_yPos()) <= 0){
            p1.set_yPos(0);
        }
        else{
            p1.update(-1);
        }
    }

    //player 2 controls and collision detection
    if (keystates[105] == true){
        //move player 2 paddle up
        if ( scale(p2.get_yPos()+p2.get_height()) >= 2){
            p2.set_yPos(400-p2.get_height());
        }
        else{
            p2.update(1);
        }
    }
    if (keystates[107] == true){
        //move player 2 paddle down
        if ( scale(p2.get_yPos()) <= 0){
            p2.set_yPos(0);
        }
        else{
            p2.update(-1);
        }
    }

    draw();
}


/**
 * @name passiveUpdate()
 * @brief This is updates events that don't need to be actively trigger, such as the balls motion.
 * Contains ball collision detection.
 */
void passiveUpdate(){
    int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
    int deltaTime = timeSinceStart - oldTimeSinceStart;
    oldTimeSinceStart = timeSinceStart;

    //ball collision and update
    if ( scale(ball.get_yPos()) <= 0){      //collision with bottom of window
        ball.set_yPos(0);
        ballYdir = 1;
    }
    if ( scale(ball.get_yPos()+ball.get_height()) >= 2 ){       //collision with top of window
        ball.set_yPos(400 - ball.get_height());
        ballYdir = -1;
    } 
    if ( ((ball.get_yPos() > p1.get_yPos()) && (ball.get_yPos() < p1.get_yPos()+p1.get_height()))  ||  ((ball.get_yPos()+ball.get_height() > p1.get_yPos()) && (ball.get_yPos()+ball.get_height() < p1.get_yPos()+p1.get_height())) ){      //collision with p1
        if ( (ball.get_xPos() <= p1.get_xPos()+p1.get_width()) && (ball.get_xPos() >= p1.get_xPos()) ){
            ball.set_xPos(p1.get_xPos()+p1.get_width());    //make sure ball doesn't go into the paddle
            ballXdir = 1;   //reverse direction
            ball.set_Xspeed(ball.get_xSpeed() + 0.1);   //increase speed
            ball.set_Yspeed(ball.get_ySpeed() + 0.1);
            xMag = ((float) rand())/(float) RAND_MAX + (rand() % 5);
        }
    }
    if ( ((ball.get_yPos() > p2.get_yPos()) && (ball.get_yPos() < p2.get_yPos()+p1.get_height()))  ||  ((ball.get_yPos()+ball.get_height() > p2.get_yPos()) && (ball.get_yPos()+ball.get_height() < p2.get_yPos()+p2.get_height())) ){      //collision with p2
        if ( (ball.get_xPos()+ball.get_width() <= p2.get_xPos()+p2.get_width()) && (ball.get_xPos()+ball.get_width() >= p2.get_xPos()) ){
            ball.set_xPos(p2.get_xPos()-ball.get_width());
            ballXdir = -1;
            ball.set_Xspeed(ball.get_xSpeed() + 0.1);   //increase speed
            ball.set_Yspeed(ball.get_ySpeed() + 0.1);
            xMag = ((float) rand())/(float) RAND_MAX + (rand() % 5);
        }
    }
    if ( scale(ball.get_xPos()+ball.get_width()) <= 0 || scale(ball.get_xPos()) >= 2){      //collision with the ends
        //imobilize the ball
        ballXdir = 0;
        ballYdir = 0;
        //determing who serves next
        if ( scale(ball.get_xPos()) <= 0){
            p2Score += 1;
            serving = 2;
        }
        else{
            p1Score += 1;
            serving = 1;
        }
        //reset ball position and speed
        ball.set_xPos(195);
        ball.set_yPos(195);
        ball.set_Xspeed(1);
        ball.set_Yspeed(1);
        xMag = 1;
    }

    ball.update(ballXdir*deltaTime/10/xMag,ballYdir*deltaTime/10);
    //cout << "x: " << ball.get_xPos() <<", y: " << ball.get_yPos() <<endl;
    draw();
}


/**
 * @name serve()
 * @brief This is the special key callback function used by glut. 
 * It detects when a F1 is pressed and performs the ball serve.
 * 
 * @param key The code of the key pressed.
 * @param x The mouse's x location when the key was pressed.
 * @param y The mouse's y location when the key was pressed.
 */
void serve(int key, int x, int y){
    if (ballXdir==0 && ballYdir==0){
        if (key == GLUT_KEY_F1){
            if(serving == 1){
                ballXdir = 1;
                ballYdir = 1;
            }
            else if (serving == 2){
                ballXdir = -1;
                ballYdir = -1;
            }
        }   
    }
}

/**
 * @name keydown()
 * @brief This is the key callback function used by glut. 
 * It detects when a key is pressed and records it.
 * Then it updates stuff based on what keys were pressed.
 * 
 * @param key The ascii code of the key pressed.
 * @param x The mouse's x location when the key was pressed.
 * @param y The mouse's y location when the key was pressed.
 */
void keydown(unsigned char key, int x, int y){
    keystates[key] = true;
    activeUpdate();
}
/**
 * @name keyup()
 * @brief This is the key up callback function used by glut. It detects when a key goes up from down and records it.
 * @param key The ascii code of the key pressed.
 * @param x The mouse's x location when the key was pressed.
 * @param y The mouse's y location when the key was pressed.
 */
void keyup(unsigned char key, int x, int y){
    keystates[key] = false;
}


/**
 * @name main()
 * @brief This is the main function.
 * @param argc
 * @param argv
 * @return 1 
 */ 
int main(int argc, char** argv){
    /*
    int argc1 = 1;
    char *argv1[1] = {(char*)"Pong"};
    */

    //Initiate GLUT and create window
    glutInit(&argc, argv);
    glutInitWindowSize(400,400);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE| GLUT_RGBA);
    glutCreateWindow("Pong");
    
    //Register callbacks
    glutDisplayFunc(draw);
    glutIdleFunc(passiveUpdate);
    glutKeyboardFunc(keydown);
    glutKeyboardUpFunc(keyup);      //registered key up callback for multi-input purposes
    glutSpecialFunc(serve);

    //recenter the camera
    glTranslatef(-1.0f,-1.0f,0.0f);   

    // Enter GLUT event processing cycle
    glutMainLoop();

    return 1;
}