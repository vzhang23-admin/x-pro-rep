/** 
 * @file Ball.cpp
 * @brief Implentation of the Ball object.
 * @author Bohr Deng 
 */

#include "Ball.h"


/** 
 * @name Ball()
 * @brief The constructor for Ball class.
 * @param xPos The x position of the ball.
 * @param yPos The y direction factor.
 * @param dx The x-axis speed of the ball.
 * @param dy The y-axis speed of the ball.
 * @param w The width of the ball.
 * @param h The width of the ball.
 */
Ball::Ball(float xPos, float yPos, float dx, float dy, float w, float h){
    x = xPos;
    y = yPos;
    xSpeed = dx;
    ySpeed = dy;
    width = w;
    height = h;
}


/** 
 * @name update()
 * @brief Updates the its position based on given factors.
 * d1 and d2 determines which direction on each axis (x,y respectively) the ball moves.
 *
 * @param d1 The x direction factor; either -1 for left, or 1 for right.
 * @param d2 The y direction factor; either 1 for up or -1 for down.
 */
void Ball::update(float d1, float d2){
    x += xSpeed * d1;
    y += ySpeed * d2;
}

/**
 * @name get_xPos()
 * @brief Ball x position getter method.
 * @return The x position of the ball.
 */ 
float Ball::get_xPos(){
    return x;
}

/**
 * @name set_xPos()
 * @brief Ball x position setter method.
 * @param xPos The x position of the ball.
 */ 
void Ball::set_xPos(float xPos){
    x = xPos;
}

/**
 * @name get_yPos()
 * @brief Ball y position getter method.
 * @return The y position of the ball.
 */ 
float Ball::get_yPos(){
    return y;
}

/**
 * @name set_yPos()
 * @brief Ball y position setter method.
 * @param yPos The y position of the ball.
 */ 
void Ball::set_yPos(float yPos){
    y = yPos;
}

/**
 * @name get_xSpeed()
 * @brief Ball x-axis speed getter method.
 * @return The x-axis speed of the ball.
 */ 
float Ball::get_xSpeed(){
    return xSpeed;
}

/**
 * @name set_Xspeed()
 * @brief Ball x-axis speed setter method.
 * @param dx The x-axis speed of the ball.
 */ 
void Ball::set_Xspeed(float dx){
    xSpeed = dx;
}

/**
 * @name get_YSpeed()
 * @brief Ball y-axis speed getter method.
 * @return The y-axis speed of the ball.
 */  
float Ball::get_ySpeed(){
    return ySpeed;
}

/**
 * @name set_Yspeed()
 * @brief Ball y-axis speed setter method.
 * @param dy The y-axis speed of the ball.
 */ 
void Ball::set_Yspeed(float dy){
    ySpeed = dy;
}

/**
 * @name get_width()
 * @brief Ball width getter method.
 * @return The width of the ball.
 */ 
float Ball::get_width(){
    return width;
}

/**
 * @name get_height()
 * @brief Ball height getter method.
 * @return The height of the ball.
 */ 
float Ball::get_height(){
    return height;
}