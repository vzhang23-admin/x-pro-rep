Pong is the classic 2D ball game where 2 players use paddles to bounce the ball back and forth. Whoever does not miss to hit the ball gains a point.

The game starts slow with the ball moving at low speed, and gets faster with every bounce from a players paddle. The ball position will reset after each round and the player who won gets to serve for the next round.

Controls:
- Player 1 move up ('w'); Player 1 move down ('s').
- Player 2 move up ('i'); Player 1 move down ('k').
- Serve (F1)
- Quit (esc)
