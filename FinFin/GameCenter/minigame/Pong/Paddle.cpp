/** 
 * @file Paddle.cpp
 * @brief Implementation of the Paddle object.
 * @author Bohr Deng
 */

#include "Paddle.h"

/** 
 * @name Paddle()
 * @brief The constructor for Paddle class.
 * @param xPos The x position of the paddle.
 * @param yPos The y direction factor.
 * @param dy The y-axis speed of the paddle.
 * @param w The width of the paddle.
 * @param h The width of the paddle.
 */
Paddle::Paddle(float xPos, float yPos, float dy, float w, float h){
    x = xPos;
    y = yPos;
    speed = dy;
    width = w;
    height = h;
}


/**
 * @name update()
 * @brief Updates the paddles position given a direction factor.
 * 
 * @param d The direction factor; 1 to move up or -1 to move down.
 */
void Paddle::update(float d){
    y += speed * d;
}

/**
 * @name get_xPos()
 * @brief Paddle x position getter method.
 * @return The x position of the paddle.
 */ 
float Paddle::get_xPos(){
    return x;
}

/**
 * @name get_yPos()
 * @brief Paddle y position getter method.
 * @return The y position of the paddle.
 */ 
float Paddle::get_yPos(){
    return y;
}

/**
 * @name set_yPos()
 * @brief Paddle y position setter method.
 * @param yPos The y position of the paddle.
 */ 
void Paddle::set_yPos(float yPos){
    y = yPos;
}

/**
 * @name get_speed()
 * @brief Paddle (y-axis) speed getter method.
 * @return The (y-axis) speed of the paddle.
 */ 
float Paddle::get_speed(){
    return speed;
}

/**
 * @name set_speed()
 * @brief Paddle (y-axis) speed setter method.
 * @param dy The (y-axis) speed of the paddle.
 */
void Paddle::set_speed(float dy){
    speed = dy;
}

/**
 * @name get_width()
 * @brief Paddle width getter method.
 * @return The width of the paddle.
 */ 
float Paddle::get_width(){
    return width;
}

/**
 * @name get_height()
 * @brief Paddle height getter method.
 * @return The height of the paddle.
 */ 
float Paddle::get_height(){
    return height;
}