/**
 * @brief main class for the Gokoku game 
 *      need two player to play with, one is white, and another one is black
 *      left click to place a piece in a empty spot of a map
 *      take turn putting the pieces
 *      whoever get five piece in a row, column, or diagnal wins.
 * @author Wenxuan
 * */

#include <iostream>
#include <GL/glut.h> 
#include <IL/il.h>
using namespace std;

//set the length and width of the game

static const  int size=19,sizeWindow=500;
//main map, 0 mean normal space, 1 means checked space, -1 means bomb, -10 mean marked bomb, 10 means marked normal space
int map[size][size];
int mousex, mousey;                     //store mousex and mousey when mouse pressed
int win=0;
int turn=-1;
int lastx=-1;
int lasty=-1;




/**
 * @brief check if player win everytimes the number change
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void checkWin(){
    int x=size*mousex/sizeWindow;
    int y=size*(sizeWindow-mousey)/sizeWindow;
    int count=0;
    int lastColor=turn*-1;
    
    //check win for x+ and x- position
    
    //count num of piece with same color, if more than five, win= color of piece
    for(int i=0;i<5;i++){
        if(map[x+i][y]==lastColor){
            count++;
        }else{
            break;
        }
    }
    for(int i=1;i<5;i++){
        if(map[x-i][y]==lastColor){
            count++;
        }else{
            break;
        }
    }
    if(count>=5){
        win=lastColor;
        return;
    }
    
    
    //check win for y+ and y- position
    count=0;
    
    
    for(int i=0;i<5;i++){
        if(map[x][y+i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    for(int i=1;i<5;i++){
        if(map[x][y-i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    cout<<count<<endl;
    if(count>=5){
        win=lastColor;
        return;
    }
    count=0;
    
    //check win for x+y+ and x-y- position
    for(int i=0;i<5;i++){
        if(map[x+i][y+i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    for(int i=1;i<5;i++){
        if(map[x-i][y-i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    if(count>=5){
        win=lastColor;
        return;
    }
    count=0;
    
    //check win for x+y- and x-y+ position
    for(int i=0;i<5;i++){
        if(map[x+i][y-i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    for(int i=1;i<5;i++){
        if(map[x-i][y+i]==lastColor){
            count++;
        }else{
            break;
        }
    }
    if(count>=5){
        win=lastColor;
        return;
    }
    count=0;
    return;
}


/**
 * @brief called in the mouse listener
 *        use to put piece in to a map and detect wins
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void doClick(){
    if(win==0){
        int x=size*mousex/sizeWindow;
        int y=size*(sizeWindow-mousey)/sizeWindow;
        if(turn==-1&&map[x][y]==0){
            map[x][y]=-1;
            lastx=x;
            lasty=y;
            turn=1;
        }
        if(turn==1&&map[x][y]==0){
            map[x][y]=1;
            lastx=x;
            lasty=y;
            turn=-1;
        }
        checkWin();
        cout<<win<<endl;
    }
        
}


/**
 * @brief use to init the map use to play the game
 * @param void
 * @return void
 * @author Wenxuan
 * */
void makeMap(){
    for(int x=0;x<size;x++){
        for(int y=0;y<size;y++){
            map[x][y]=0;
        }
    }
}

    
/**
 * @brief use to display the game
 * @param void
 * @return void
 * @author Wenxuan
 * */
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    

    glBegin(GL_QUADS);
    glColor3f(1.0f, 1.0f, 1.0f);  //white
    glVertex2f(0,0);     
    glVertex2f(size,0);   
    glVertex2f(size,size); 
    glVertex2f(0,size);
    //draw the indecation of the piece that had been last put in
    glColor3f(1.0f, 0.0f, 0.0f);  //greed
    if(win!=0){
        glColor3f(0.0f, 1.0f, 0.0f);  //red
    }
    glVertex2f(lastx,lasty);     
    glVertex2f(lastx,lasty+1);   
    glVertex2f(lastx+1,lasty+1); 
    glVertex2f(lastx+1,lasty);
     //draw the board
    for(int x=0;x<size;x++){
        glColor3f(0.0f, 0.0f, 0.0f);  
       
        glVertex2f(x+0.48,0);     
        glVertex2f(x+0.48,size);   
        glVertex2f(x+0.52,size); 
        glVertex2f(x+0.52, 0); 
        
        glVertex2f(0, x+0.48);     
        glVertex2f(size, x+0.48);   
        glVertex2f(size, x+0.52); 
        glVertex2f(0, x+0.52); 

    }


    //draw the piece
    for(int x=0;x<size;x++){
        for(int y=0;y<size;y++){  
            if(map[x][y]==-1){
                glColor3f(0.0f, 0.0f, 0.0f);  
            }if(map[x][y]==1){
                 glColor3f(0.8f, 0.8f, 0.8f);  
            }
                
            if(map[x][y]!=0){
                
                glVertex2f(x+0.2,y+0.2);     
                glVertex2f(x+0.2,y+0.8);   
                glVertex2f(x+0.8,y+0.8); 
                glVertex2f(x+0.8,y+0.2); 
               
            }
        }      
        
    }
    glEnd();
    glFlush();
    
    
}


/**
 * @brief the mouse listener, call mouseClick method when mouse click
 * @param int button which button of the mouse
 *            int state is it push in or release
 *            int x mouse x position
 *            int y mouse y position
 * @return void
 * @author Wenxuan
 * */
void myMouseFunc(int button, int state, int x, int y){ 
    if(win!=0){
            return;
    }
	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		mousex=x;
        mousey=y;
		doClick();
        //loop
        glutPostRedisplay(); 
	}
}

/** 
 * overwrite
 * @brief to make sure user will not reshape the window
 * @param GLsizei newwidth: the passin parameter, GLsizei newheight: the passin parameter
 *              both parameter are not used in here, since we don't want to resize the window.
 * @return void
 * @author Wenxuan Zhang
 * */
void reshape(GLsizei newwidth, GLsizei newheight) 
{  
    glutReshapeWindow(sizeWindow,sizeWindow);
}


/** 
 * @brief clear the board on init gl
 *      also set the vertex and viewport for gl
 * @param void
 * @return void
 * @author Wenxuan
 * */
void initGL(){
     glViewport(0, 0, sizeWindow,sizeWindow);              //use a screen size of WIDTH x HEIGHT
     glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
     gluOrtho2D(0,size,0,size);                                   //set the sceen coordinate from 0 to 50
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
    }

int main (int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    makeMap();
    //make the game window
    int argc1 = 1;
    char *argv1[1]={(char*)"minesweeper"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Games"); 
    //add displayfunction, mouse listener, and init the gl
    glutDisplayFunc(display); 
    glutMouseFunc(myMouseFunc);
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    initGL();
     
    glutMainLoop();
}
