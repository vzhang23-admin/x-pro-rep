\hypertarget{main_8cpp}{}\section{main.\+cpp File Reference}
\label{main_8cpp}\index{main.\+cpp@{main.\+cpp}}


Main class for the maze game. Use wsad to move(w for up, s for down, a for left, d for right). Red is the wall, blue is the path, and white is the character. Starts from the left position, and the goal is to the right where the exit is.  


{\ttfamily \#include $<$stdlib.\+h$>$}\newline
{\ttfamily \#include $<$stdio.\+h$>$}\newline
{\ttfamily \#include $<$iostream$>$}\newline
{\ttfamily \#include $<$algorithm$>$}\newline
{\ttfamily \#include $<$G\+L/glut.\+h$>$}\newline
{\ttfamily \#include $<$math.\+h$>$}\newline
Include dependency graph for main.\+cpp\+:
% FIG 0
\subsection*{Functions}
\begin{DoxyCompactItemize}
\item 
void \hyperlink{main_8cpp_ab7b9d772bc10b720ac4e8165a5c434b4}{make\+Path\+D\+FS} (int i, int j)
\begin{DoxyCompactList}\small\item\em make\+Path\+D\+FS, a method which uses D\+FS to make path for maze. \end{DoxyCompactList}\item 
\mbox{\Hypertarget{main_8cpp_af27e3d03fd72a5a3cdcbe38894907692}\label{main_8cpp_af27e3d03fd72a5a3cdcbe38894907692}} 
void \hyperlink{main_8cpp_af27e3d03fd72a5a3cdcbe38894907692}{make\+Maze} ()
\begin{DoxyCompactList}\small\item\em \hyperlink{main_8cpp_af27e3d03fd72a5a3cdcbe38894907692}{make\+Maze()}, inits the maze board and makes a path. \end{DoxyCompactList}\item 
void \hyperlink{main_8cpp_aef7ba2f69afb2d954545f64c7fe24b14}{keyboard} (unsigned char key, int x, int y)
\begin{DoxyCompactList}\small\item\em \hyperlink{main_8cpp_aef7ba2f69afb2d954545f64c7fe24b14}{keyboard()}; this is the keyboard listener method that is called by glut. \end{DoxyCompactList}\item 
\mbox{\Hypertarget{main_8cpp_a1e5b20fed15743656bb6d2e6a6ea6269}\label{main_8cpp_a1e5b20fed15743656bb6d2e6a6ea6269}} 
void \hyperlink{main_8cpp_a1e5b20fed15743656bb6d2e6a6ea6269}{display} ()
\begin{DoxyCompactList}\small\item\em \hyperlink{main_8cpp_a1e5b20fed15743656bb6d2e6a6ea6269}{display()}; this methods displays everthing to the screen \end{DoxyCompactList}\item 
\mbox{\Hypertarget{main_8cpp_a12791d9e49a2fd3306290a226864aba4}\label{main_8cpp_a12791d9e49a2fd3306290a226864aba4}} 
void {\bfseries init\+GL} ()
\item 
int \hyperlink{main_8cpp_a3c04138a5bfe5d72780bb7e82a18e627}{main} (int argc, char $\ast$$\ast$argv)
\begin{DoxyCompactList}\small\item\em \hyperlink{main_8cpp_a3c04138a5bfe5d72780bb7e82a18e627}{main()}; this is main function, all the glut setup is here. \end{DoxyCompactList}\end{DoxyCompactItemize}
\subsection*{Variables}
\begin{DoxyCompactItemize}
\item 
\mbox{\Hypertarget{main_8cpp_a241d2d1738cb8e8212be0c42360a041f}\label{main_8cpp_a241d2d1738cb8e8212be0c42360a041f}} 
bool {\bfseries finish\+Main\+Path} =0
\item 
\mbox{\Hypertarget{main_8cpp_a7e9106e0538c1f43862ad6076520f330}\label{main_8cpp_a7e9106e0538c1f43862ad6076520f330}} 
bool {\bfseries maze} \mbox{[}length\mbox{]}\mbox{[}width\mbox{]}
\item 
\mbox{\Hypertarget{main_8cpp_a8c23d2347ae29ee9a05a4c86c81f938b}\label{main_8cpp_a8c23d2347ae29ee9a05a4c86c81f938b}} 
int {\bfseries currentx} =0
\item 
\mbox{\Hypertarget{main_8cpp_a49a49ec4bc9f694f8d02d340582859e2}\label{main_8cpp_a49a49ec4bc9f694f8d02d340582859e2}} 
int {\bfseries currenty} =width/2
\end{DoxyCompactItemize}


\subsection{Detailed Description}
Main class for the maze game. Use wsad to move(w for up, s for down, a for left, d for right). Red is the wall, blue is the path, and white is the character. Starts from the left position, and the goal is to the right where the exit is. 

\begin{DoxyAuthor}{Author}
Bohr Deng 
\end{DoxyAuthor}


\subsection{Function Documentation}
\mbox{\Hypertarget{main_8cpp_aef7ba2f69afb2d954545f64c7fe24b14}\label{main_8cpp_aef7ba2f69afb2d954545f64c7fe24b14}} 
\index{main.\+cpp@{main.\+cpp}!keyboard@{keyboard}}
\index{keyboard@{keyboard}!main.\+cpp@{main.\+cpp}}
\subsubsection{\texorpdfstring{keyboard()}{keyboard()}}
{\footnotesize\ttfamily void keyboard (\begin{DoxyParamCaption}\item[{unsigned char}]{key,  }\item[{int}]{x,  }\item[{int}]{y }\end{DoxyParamCaption})}



\hyperlink{main_8cpp_aef7ba2f69afb2d954545f64c7fe24b14}{keyboard()}; this is the keyboard listener method that is called by glut. 


\begin{DoxyParams}{Parameters}
{\em key} & the ascii code of the key pressed \\
\hline
{\em x} & the mouse cursors x location when key was pressed \\
\hline
{\em y} & the mouse cursors y location when key was pressed \\
\hline
\end{DoxyParams}
\mbox{\Hypertarget{main_8cpp_a3c04138a5bfe5d72780bb7e82a18e627}\label{main_8cpp_a3c04138a5bfe5d72780bb7e82a18e627}} 
\index{main.\+cpp@{main.\+cpp}!main@{main}}
\index{main@{main}!main.\+cpp@{main.\+cpp}}
\subsubsection{\texorpdfstring{main()}{main()}}
{\footnotesize\ttfamily int main (\begin{DoxyParamCaption}\item[{int}]{argc,  }\item[{char $\ast$$\ast$}]{argv }\end{DoxyParamCaption})}



\hyperlink{main_8cpp_a3c04138a5bfe5d72780bb7e82a18e627}{main()}; this is main function, all the glut setup is here. 


\begin{DoxyParams}{Parameters}
{\em argc} & \\
\hline
{\em argv} & \\
\hline
\end{DoxyParams}
\mbox{\Hypertarget{main_8cpp_ab7b9d772bc10b720ac4e8165a5c434b4}\label{main_8cpp_ab7b9d772bc10b720ac4e8165a5c434b4}} 
\index{main.\+cpp@{main.\+cpp}!make\+Path\+D\+FS@{make\+Path\+D\+FS}}
\index{make\+Path\+D\+FS@{make\+Path\+D\+FS}!main.\+cpp@{main.\+cpp}}
\subsubsection{\texorpdfstring{make\+Path\+D\+F\+S()}{makePathDFS()}}
{\footnotesize\ttfamily void make\+Path\+D\+FS (\begin{DoxyParamCaption}\item[{int}]{i,  }\item[{int}]{j }\end{DoxyParamCaption})}



make\+Path\+D\+FS, a method which uses D\+FS to make path for maze. 


\begin{DoxyParams}{Parameters}
{\em i} & x coordinate of the maze \\
\hline
{\em j} & y coordingate of the maze \\
\hline
\end{DoxyParams}
