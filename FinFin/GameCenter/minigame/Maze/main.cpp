/** 
 * @file main.cpp
 * @brief Main class for the maze game. 
 * Use wsad to move(w for up, s for down, a for left, d for right).
 * Red is the wall, blue is the path, and white is the character.
 * Starts from the left position, and the goal is to the right where the exit is.
 * 
 * @author Bohr Deng
 */


#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <stdio.h> 
#include <GL/glut.h> 
#include <math.h> 

using namespace std;

//set the length and width of the maze
static const  int length=50, width=50;

//flag for checking if can reach the goal or not
bool finishMainPath=0;

//main maze
bool maze[length][width];

//current position
int currentx=0, currenty=width/2;

/**
 * @brief makePathDFS, a method which uses DFS to make path for maze.
 * @param i x coordinate of the maze
 * @param j y coordingate of the maze
 *
 * */
void makePathDFS ( int i,int  j){
    
    //start position
    if(i==0 && j==width/2){
        maze[i][j]=0;
        makePathDFS ( i+1, j);
        
        return;
    }
    
    //end position
    if(finishMainPath==0&& i==length-1){
        maze[i][j]=0;
        finishMainPath=1;
        
        return;
    }
    
    //out of bound
    if(i<1 || j<1 || i>length-2||j>width-2){
        return;
    }
    
    //make sure the area is not too open(maze is not too easy)
    if(maze[i][j+1]+maze[i][j-1]+maze[i+1][j]+maze[i-1][j]<3){
        if( rand()%100<97){
            return;
        }
    } 
    
    //random path
    maze[i][j]=0;
    int next[4][3]={{rand()%100,i, j+1},{rand()%100,i, j-1},{rand()%100,i+1,j},{rand()%100,i-1,j}};
    for(int x=0;x<4;x++){
        for(int y=0;y<3;y++){
            if(next[y][0]>next[y+1][0]){
                int temp;
                temp=next[y][0];
                next[y][0]=next[y+1][0];
                next[y+1][0]=temp;
                temp=next[y][1];
                next[y][1]=next[y+1][1];
                next[y+1][1]=temp;
                temp=next[y][2];
                next[y][2]=next[y+1][2];
                next[y+1][2]=temp;
            }
        }
    }
    
    //make path using dfs
    for(int i=0;i<4;i++){
        if((!finishMainPath)||rand()%100<90){ 
            makePathDFS(next[i][1], next[i][2]);
       }
    }
    return;  
}


/**
 * @brief makeMaze(), inits the maze board and makes a path.
 * 
 */ 
void makeMaze(){
    
    int i=length, j=width;
    for(int x=0;x<i;x++){
        for(int y=0;y<j;y++){
            maze[x][y]=1;
        }
    }
    
    makePathDFS( 0, j/2);
    
}


//key listener method
/**
 * @brief keyboard(); this is the keyboard listener method that is called by glut.
 * @param key the ascii code of the key pressed
 * @param x the mouse cursors x location when key was pressed
 * @param y the mouse cursors y location when key was pressed
 * 
 */ 
void keyboard(unsigned char key, int x, int y) { 
    if(key=='w'&&maze[currentx][currenty+1]==0){
        
    currenty++;  
    }
    if(key=='a'&&maze[currentx-1][currenty]==0){
        
    currentx--;  
    }
    if(key=='s'&&maze[currentx][currenty-1]==0){
        
    currenty--;  
    }
    if(key=='d'&&maze[currentx+1][currenty]==0){
        
    currentx++;  
    }
    
    //update display
    glutPostRedisplay(); 
} 
    

/**
 * @brief display(); this methods displays everthing to the screen
 * 
 */ 
void display() {
    
    //clear the board, draw the wall 
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_QUADS);
    float changex=2.0f/length;
    float changey=2.0f/width;
    for(int x=0;x<length;x++){
        for(int y=0;y<width;y++){
            if(maze[x][y]){
                glColor3f(1.0f, 0.0f, 0.0f);
            }else{
                glColor3f(0.0f, 0.0f, 1.0f);
            }
            float vertx=x*changex-1;
            float verty=y*changey-1;
            glVertex2f(vertx,verty);     
            glVertex2f(vertx+changex,verty);   
            glVertex2f(vertx+changex,verty+changey); 
            glVertex2f(vertx,verty+changey); 
        }
    }
    
    
    //draw the character
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2f(currentx*changex-1,currenty*changey-1);     
    glVertex2f(currentx*changex-1+changex,currenty*changey-1);   
    glVertex2f(currentx*changex-1+changex,currenty*changey-1+changey); 
    glVertex2f(currentx*changex-1,currenty*changey-1+changey); 
    glEnd();
    
    //end the game if goal reached
    if (currentx>=length){
        printf("Game Over !!!\n YOU WIN\n"); 
        exit(0); 
    }
    glFlush();
}

//clear the board on init gl
void initGL(){
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

/**
 * @brief main(); this is main function, all the glut setup is here.
 * @param argc
 * @param argv
 * 
 */ 
int main (int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    
    //make maze
    makeMaze();
    
    //make the game window
    int argc1 = 1;
    char *argv1[1]={(char*)"Maze"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Game");  
    
    //add displayfunction, keyboard listener, and init the gl
    glutDisplayFunc(display); 
    glutKeyboardFunc(keyboard); 
    initGL();
    
    //reshape window, start the game
    int maxSize=max(length, width);
    glutReshapeWindow((500*length)/maxSize,(500*width)/maxSize);
    glutMainLoop();
}
