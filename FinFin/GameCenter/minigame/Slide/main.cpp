/**
 * @brief main class for the slider puzzle game 
 *        make the number order from 1 to 15 and empty space at the end
 *        order like this:
 *        1  2  3  4
 *        5  6  7  8
 *        9  10 11 12
 *        13 14 15
 * @author Wenxuan
 * */

#include <iostream>
#include <GL/glut.h> 
#include <IL/il.h>
using namespace std;

//set the length and width of the game

static const  int size=4,sizeWindow=500, steps=70;
//main map, 0 mean normal space, 1 means checked space, -1 means bomb, -10 mean marked bomb, 10 means marked normal space
int map[size][size];
int mousex, mousey;                     //store mousex and mousey when mouse pressed
int win=0;
GLuint texid[11];                        //texture position, we need one for flag and also for 8 numbers, one for bomb, and one for win image


/**
 * @brief check if player win everytimes the number change
 * @param void
 * @return bool: true for player win, false for player did not win
 * @author Wenxuan
 * */
bool checkWin(){
    int x=0;
    int y=0;
    int num;
    for(int i=0;i<size*size-1;i++){
        num=i+1;
        if(num!=map[x][size-1-y]){
            cout<<num<<endl;
            return false;
        }
        
        x++;
        if(x==size){
            x=0;
            y++;
        }
         
    }
    cout<<"Win"<<endl;
    return true;
}

}
/**
 * @brief called in the mouse listener
 *         use when user Leftclick to slide a number to empty slot
 * @param void
 * @return void
 * @author Wenxuan
 * */
void doLeftClick(){
    int x=size*mousex/sizeWindow;
    int y=size*(sizeWindow-mousey)/sizeWindow;
    cout<<x<<":"<<y<<endl;
    cout<<map[x][y]<<endl;
    if(y<size-1&&map[x][y+1]==0){
        map[x][y+1]=map[x][y];
        map[x][y]=0;
    }
    if(y>0&&map[x][y-1]==0){
        map[x][y-1]=map[x][y];
        map[x][y]=0;
    }
    if(x<size-1&&map[x+1][y]==0){
        map[x+1][y]=map[x][y];
        map[x][y]=0;
    }
    if(x>0&&map[x-1][y]==0){
        map[x-1][y]=map[x][y];
        map[x][y]=0;
    }
    if(checkWin()){
            win=1;
    }
    
}



/**
 * @brief use to init the map use to play the game
 *        recurse through all position of the map to build a sudoku
 * @param int x: current x position to recurse
 *            int y: current y position to recurse
 * @return bool true for finish the map, false for unable to finish the map
 * @author Wenxuan
 * */
void makeMap(){
    
    for(int x=0;x<size;x++){
        for(int y=0;y<size;y++){
            map[x][y]=0;
        }
    }
    
    
    int x;
    int y;
    for(int i=0;i<size*size;i++){
        x=i%size;
        y=size-1-i/size;
        map[x][y]=i+1;
    }
    map[size-1][0]=0;
    x=size-1;
    y=0;
    for(int i=0;i<steps;i++){
        
        int dirc=rand()%4;
        int dircArr[4][2]={{1,0},{-1,0},{0,1},{0,-1}};
        //0=up, 1=down,2=right, 3=left
        while(!(x+dircArr[dirc][0]>=0&&x+dircArr[dirc][0]<size&&y+dircArr[dirc][1]>=0&&y+dircArr[dirc][1]<size)){
            dirc=rand()%4;
        }
       
        int newx=x+dircArr[dirc][0];
        int newy=y+dircArr[dirc][1];
        int temp= map[x][y];
        map[x][y]=map[newx][newy];
        map[newx][newy]=temp;
        
        x=newx;
        y=newy;
    }
    
}

    
/**
 * @brief use to display the game
 * @param void
 * @return void
 * @author Wenxuan
 * */
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, 0);

    for(int x=0;x<size;x++){
            for(int y=0;y<size;y++){
                int tex=map[x][y];
                if(map[x][y]==0&&win==0){
                    
                    glBindTexture(GL_TEXTURE_2D, 0); 
                    glColor3f(1.0f, 1.0f, 1.0f);//white color
                    glBegin(GL_QUADS); 
                    glTexCoord2i(x, y);glVertex2f(x,y);     
                    glTexCoord2i(x+1, y);glVertex2f(x+1,y);   
                    glTexCoord2i(x+1, y+1);glVertex2f(x+1, y+1); 
                    glTexCoord2i(x, y+1);glVertex2f(x, y+1); 
                    glEnd();   
                     
                } else{
                    if(tex==0){
                        tex=size*size;
                    }
                    glBindTexture(GL_TEXTURE_2D, texid[tex]); 
                    glBegin(GL_QUADS); 
                    glTexCoord2i(x, y);glVertex2f(x,y);     
                    glTexCoord2i(x+1, y);glVertex2f(x+1,y);   
                    glTexCoord2i(x+1, y+1);glVertex2f(x+1, y+1); 
                    glTexCoord2i(x, y+1);glVertex2f(x, y+1); 
                    glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
                    glEnd();
                }
            }
        }
    
    glFlush();
    
    
}
/**
 * @brief the mouse listener, call mouseClick method when mouse click
 * @param int button which button of the mouse
 *            int state is it push in or release
 *            int x mouse x position
 *            int y mouse y position
 * @return void
 * @author Wenxuan
 * */
void myMouseFunc(int button, int state, int x, int y){
    if(win!=0){
            return;
    }
	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		mousex=x;
        mousey=y;
		doLeftClick();
        glutPostRedisplay(); 
	}
}
/**
 * @brief it load the image in the given filename using devil
 *      return -1 if error
 * @param char *filename filename to load from
 * @return int -1 if error
 * @author Wenxuan
 * */
int LoadImage(char *filename)
{
    ILboolean success; 
    ILuint image; 
    ilGenImages(1, &image); // Generation of one image name
    ilBindImage(image); //Binding of image name
    success = ilLoadImage(filename); // Loading of the image filename by DevIL
    if (success){
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); 
        if (!success){
            return -1;
        }
    }else{
        return -1;
    }
    return image;
}

/** 
 * @brief use to call LoadImage method, it will load the filename texture to the given index
 * @param string filename: the filename that this method will load, 
 *             int index: index of where will the texture be load to
 * @return void
 * @author Wenxuan
 * */
void autoLoad(string fileName, int index){  
    
    /* load the file picture with DevIL */
    int  image = LoadImage(&fileName[0]);
    if ( image == -1 )
    {
        printf("Can't load picture file by DevIL ");
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
       glGenTextures(11, &texid[index]); /* Texture name generation */
       glBindTexture(GL_TEXTURE_2D, texid[index]); /* Binding of texture name */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
       glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 
        0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */
    
    
}

/** 
 * overwrite
 * @brief to make sure user will not reshape the window
 * @param GLsizei newwidth: the passin parameter, GLsizei newheight: the passin parameter
 *              both parameter are not used in here, since we don't want to resize the window.
 * @return void
 * @author Wenxuan Zhang
 * */
void reshape(GLsizei newwidth, GLsizei newheight) 
{  
    glutReshapeWindow(sizeWindow,sizeWindow);
}


/** 
 * @brief clear the board on init gl
 *      also set the vertex and viewport for gl
 * @param void
 * @return void
 * @author Wenxuan
 * */
void initGL(){
     glViewport(0, 0, sizeWindow,sizeWindow);              //use a screen size of WIDTH x HEIGHT
     glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
     gluOrtho2D(0,size,0,size);                                   //set the sceen coordinate from 0 to 50
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
    }

int main (int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    makeMap();
    
    //make the game window
    int argc1 = 1;
    char *argv1[1]={(char*)"minesweeper"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Games"); 
    //add displayfunction, mouse listener, and init the gl
    glutDisplayFunc(display); 
    glutMouseFunc(myMouseFunc);
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    initGL();
    //check if the user have the correct DevIL version
     if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
     {
           printf("wrong DevIL version ");
           return -1;
     }
     
    //init the DevIL, load the texture
    ilInit(); 
    autoLoad("one.jpg",1);
    autoLoad("two.jpg",2);
    autoLoad("three.jpg",3);
    autoLoad("four.jpg",4);
    autoLoad("five.jpg",5);
    autoLoad("six.jpg",6);
    autoLoad("seven.jpg",7);
    autoLoad("eight.jpg",8);
    autoLoad("nine.jpg",9);
    autoLoad("ten.jpg",10);
    autoLoad("eleven.jpg",11);
    autoLoad("twelve.jpg",12);
    autoLoad("thirteen.jpg",13);
    autoLoad("fourteen.jpg",14);
    autoLoad("fifteen.jpg",15);
    autoLoad("sixteen.jpg",16);
    glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
    glutMainLoop();
}
