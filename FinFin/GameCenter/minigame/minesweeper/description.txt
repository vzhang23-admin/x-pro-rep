Minesweeper is a single-player puzzle video game. The objective of the game is to clear a rectangular board containing hidden "mines" or bombs without detonating any of them, with help from clues about the number of neighboring mines in each field. The game originates from the 1960s, and it has been written for many computing platforms in use today. It has many variations and offshoots.

Use left click to explore one area, if the area is bomb, then game over.

If the area explore is a safe spot, it will show you how many bomb are near by(0 to 8).

Right click to put a flag on a spot, putting a flag will not tell you is this place safe or not, right click on the flag to remove it.

to win the game, you need to explore all safe place, and flag all bomb.

