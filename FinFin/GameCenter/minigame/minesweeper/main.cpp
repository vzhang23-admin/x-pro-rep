
 /**
  * @file main.cpp
  * @brief main class for the minsweeper game
  * @details use right click to mark bomb, left click to check if cell is bomb or not,
  *          the cell will display the number of nearby bomb.   
  *          Mark all bomb and check all non-bomb cell to win.
  * @author
  * @date 2020-11-27
  **/



#include <iostream>
#include <GL/glut.h> 
#include <IL/il.h>
using namespace std;

//set the length and width of the game

static const  int size = 10, mineNum = 10, sizeWindow = 500;
//main map, 0 mean normal space, 1 means checked space, -1 means bomb, -10 mean marked bomb, 10 means marked normal space
int map[size][size];
int mousex, mousey;                     //store mousex and mousey when mouse pressed
int win = 0;
GLuint texid[11];                        //texture position, we need one for flag and also for 8 numbers, one for bomb, and one for win image

 /**
 * @brief checkWin() is to check if the player wins the game
 * @param no parameter needed for this method
 * @return true if the player wins, otherwise false
 * @author Xinye Xu
 * 
 * 
 **/
bool checkWin() {
    for (int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++) {
            if (map[x][y] == 0 || map[x][y] == 10 || map[x][y] == -1) {
                return false;
            }
        }
    }
    return true;
}
/**
 * @breif doRightClick() method called in the mouse listener, used to make flag on cells
 * @param no parameter needed
 * @return void type, returns nothing
 * @author Xinye Xu
 *
 **/
void doRightClick() {
    int realx = size * mousex / sizeWindow;
    int realy = size * (sizeWindow - mousey) / sizeWindow;
    if (map[realx][realy] == -1) {
        map[realx][realy] = -10;
    }
    else if (map[realx][realy] == 0) {
        map[realx][realy] = 10;
    }
    else if (map[realx][realy] == 10) {
        map[realx][realy] = 0;
    }
    else if (map[realx][realy] == -10) {
        map[realx][realy] = -1;
    }
    if (checkWin()) {
        win = 1;
    }

}
/**
 * @brief doLeftClock() method called in the mouse listener, used when user check cells are normal or not
 * @param no parameter needed
 * @return void type, returns nothing
 * @author Xinye XU
 * */
void doLeftClick() {
    int realx = size * mousex / sizeWindow;
    int realy = size * (sizeWindow - mousey) / sizeWindow;
    if (map[realx][realy] == -1 || map[realx][realy] == -10) {
        win = -1;
        cout << "You lose" << endl;
    }
    else {
        map[realx][realy] = 1;
    }
    if (checkWin()) {
        win = 1;
    }

}

/**
*  @brief spawnMine() is to generate or regenerate a mine
*  @param no parameter needed
*  @return void type, returns nothing
*  @author Xinye Xu
**/
void spawnMine() {
    int x = rand() % size;
    int y = rand() % size;
    while (map[x][y] != 0) {//make sure it spawn on empty space
        x = rand() % size;
        y = rand() % size;
    }
    map[x][y] = -1;
}

/**
* @brief makeMap() is to make up a map for the game, and spawn the mine inside the map as well
* @param no parameter needed
* @return void type
* @author Xinye Xu
**/
void makeMap() {
    for (int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++) {
            map[x][y] = 0;//initialize each slot of map to zero
        }
    }
    for (int i = 0; i < mineNum; i++) {
        spawnMine();
    }
}


/**
* @brief display() is to display the it will display the windows that contains the game
* @param no parameters needed
* @return no return value
* @author Xinye XU
**/
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, 0);
    //began the gl
    if (win == 0) {
        glBegin(GL_QUADS);

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (map[x][y] == 0 || map[x][y] == -1) {
                    glColor3f(0.0f, 0.0f, 0.0f);  //black
                    //draw the square, use as seperation between two cells
                    glVertex2f(x, y);
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);

                    glColor3f(1.0f, 1.0f, 1.0f);  //white
                    //draw the square
                    glVertex2f(x + 0.05, y + 0.05);
                    glVertex2f(x + 0.9, y + 0.05);
                    glVertex2f(x + 0.9, y + 0.9);
                    glVertex2f(x + 0.05, y + 0.9);
                }
                else if (map[x][y] == 1) {
                    //if it has been discovered, draw it as completly white
                    glColor3f(1.0f, 1.0f, 1.0f);  //white
                    //draw the square
                    glVertex2f(x, y);
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }

        }
        glEnd();//end gl for draw the map



    //draw the texture of the discovered part and the flag
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {

                //since flag texture is 0 and no texture for the number 0, 
                //init the texture as a flag texture
                int tex = 0;
                int draw = 0;//if draw is 0, it means this cells is a dicovered cell with 0 bomb nearby, don't draw the texture
                if (map[x][y] == -10 || map[x][y] == 10) {
                    draw = 1;//make draw 1, so it will draw texture
                }if (map[x][y] == 1) {

                    //get all the bomb nearby, the number of bomb reperent the id of texture
                    for (int i = -1; i <= 1; i++) {
                        for (int e = -1; e <= 1; e++) {
                            if ((e != 0 || i != 0) && x + i >= 0 && x + i < size && y + e >= 0 && y + e < size && (map[x + i][y + e] == -1 || map[x + i][y + e] == -10)) {
                                draw = 1;
                                tex++;
                            }
                        }
                    }
                }

                //if it is not undiscovered and draw is one, draw the texture
                if (map[x][y] != -1 && map[x][y] != 0 && draw == 1) {
                    glBindTexture(GL_TEXTURE_2D, texid[tex]);
                    glBegin(GL_QUADS);
                    glTexCoord2i(x, y); glVertex2f(x, y);
                    glTexCoord2i(x + 1, y); glVertex2f(x + 1, y);
                    glTexCoord2i(x + 1, y + 1); glVertex2f(x + 1, y + 1);
                    glTexCoord2i(x, y + 1); glVertex2f(x, y + 1);
                    glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
                    glEnd();
                }
            }
        }
    }
    else  if (win == -1) { //if lose, it will display all bomb and cells

       //for all cells, count num of bomb nearby
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int tex = 0;
                for (int i = -1; i <= 1; i++) {
                    for (int e = -1; e <= 1; e++) {
                        if ((e != 0 || i != 0) && x + i >= 0 && x + i < size && y + e >= 0 && y + e < size && (map[x + i][y + e] == -1 || map[x + i][y + e] == -10)) {
                            tex++;
                        }
                    }
                }
                //if this is bomb cells, tex is 9, represent bomb
                if (map[x][y] == -1 || map[x][y] == -10) {
                    tex = 9;
                }
                //change the texture and draw it
                glBindTexture(GL_TEXTURE_2D, texid[tex]);
                if (tex == 0) {//if texture is 0, this mean  it have no bomb nearby, draw white space

                    glBindTexture(GL_TEXTURE_2D, 0);
                    glBegin(GL_QUADS);
                    glColor3f(1.0f, 1.0f, 1.0f);
                    glVertex2f(x, y);
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                    glEnd();
                }
                else {
                    glBegin(GL_QUADS);
                    glTexCoord2i(x, y); glVertex2f(x, y);
                    glTexCoord2i(x + 1, y); glVertex2f(x + 1, y);
                    glTexCoord2i(x + 1, y + 1); glVertex2f(x + 1, y + 1);
                    glTexCoord2i(x, y + 1); glVertex2f(x, y + 1);
                    glBindTexture(GL_TEXTURE_2D, 0);
                    glEnd();
                }
            }
        }
    }
    else if (win == 1) {//if win, draw win image

        glBindTexture(GL_TEXTURE_2D, texid[10]);
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0); glVertex2f(0, 0);
        glTexCoord2i(15, 0); glVertex2f(50, 0);
        glTexCoord2i(15, 15); glVertex2f(50, 50);
        glTexCoord2i(0, 15); glVertex2f(0, 50);
        glBindTexture(GL_TEXTURE_2D, 0);
        glEnd();
    }
    glFlush();


}
/**
 * @brief myMouseFunc() the mouse listener call mouseClick method when mouse click
 * @param no parameter needed 
 * @return no return value
 * @author Xinye XU
 * */
void myMouseFunc(int button, int state, int x, int y) {
    if (win != 0) {
        return;
    }
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        mousex = x;
        mousey = y;
        doLeftClick();
        //loop
        glutPostRedisplay();
    }
    else  if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
        mousex = x;
        mousey = y;
        doRightClick();
        //loop
        glutPostRedisplay();
    }
}
/**
 * @brief LoadImage() method Load an image using DevIL
 * @return -1 if error occurs
 * @param the file name for the desired image to display
 * @author Wenxuan Zhang
 * */
int LoadImage(char* filename)
{
    ILboolean success;
    ILuint image;
    ilGenImages(1, &image); // Generation of one image name
    ilBindImage(image); //Binding of image name
    success = ilLoadImage(filename); // Loading of the image filename by DevIL
    if (success) {
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
        if (!success) {
            return -1;
        }
    }
    else {
        return -1;
    }
    return image;
}

/**
 * @brief autoLoad() method, used to call LoadImage method, it will load the filename texture to the given index
 * @param1: string type, name for the file
 * @param2: int type, index of the file 
 * @return no return value
 * @author Wenxuan Zhang
 **/
void autoLoad(string fileName, int index) {

    /* load the file picture with DevIL */
    int  image = LoadImage(&fileName[0]);
    if (image == -1)
    {
        printf("Can't load picture file by DevIL ");
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
    glGenTextures(11, &texid[index]); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, texid[index]); /* Binding of texture name */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
    glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
        0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */


}



/**
 * @brief reshape function, to make sure user will not reshape the window
 * @param: newwidth, newheight, both are window size parameter
 * @return no return value
 * @author Wenxuan Zhang
 * 
 **/
void reshape(GLsizei newwidth, GLsizei newheight)
{
    glutReshapeWindow(sizeWindow, sizeWindow);
}
/**
* @breif initGl() method is to clear the board on inti gl
* @param: no parameters needed
* @return no return values
* @author Xinye XU
* 
**/

void initGL() {
    glViewport(0, 0, sizeWindow, sizeWindow);              //use a screen size of WIDTH x HEIGHT
    glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
    gluOrtho2D(0, size, 0, size);                                   //set the sceen coordinate from 0 to 50
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
}

/**
* @breif main() function is to 
* @details the only entry of this mini game
* @param1 argc number of arguments parameter
* @param2 argv command arguments array pointer
* @return no return values 
* @author Xinye XU
**/
int main(int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    makeMap();
    //make the game window
    int argc1 = 1;
    char* argv1[1] = { (char*)"minesweeper" };
    glutInit(&argc1, argv1);
    glutCreateWindow("Games");
    //add displayfunction, mouse listener, and init the gl
    glutDisplayFunc(display);
    glutMouseFunc(myMouseFunc);
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    initGL();
    //check if the user have the correct DevIL version
    if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
    {
        printf("wrong DevIL version ");
        return -1;
    }

    //init the DevIL, load the texture
    ilInit();
    autoLoad("flag.jpg", 0);
    autoLoad("one.jpg", 1);
    autoLoad("two.jpg", 2);
    autoLoad("three.jpg", 3);
    autoLoad("four.jpg", 4);
    autoLoad("five.jpg", 5);
    autoLoad("six.jpg", 6);
    autoLoad("seven.jpg", 7);
    autoLoad("eight.jpg", 8);
    autoLoad("bomb.jpg", 9);
    autoLoad("win.jpg", 10);
    glBindTexture(GL_TEXTURE_2D, 0); //switch texture to 0
    glutMainLoop();
}
