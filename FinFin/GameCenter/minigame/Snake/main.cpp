/**
 * @brief main class for the snake game 
 *      use wsad to move(w for up, s for down, a for left, d for right)
 *      red is the wall, blue is the path, and white is the character
 *      start from the left position, and the goal is to exit through the only right opening
 * @author Wenxuan
 * */


#include <iostream>
#include <GL/glut.h> 
#include <IL/il.h>
#include <vector>
#include <tuple>
#include <time.h>
using namespace std;

//set the length and width of the game

static const  int size=10;
int dirc=0;
int snakeLength=1;
//if is 0, game lose
bool continu=1;
tuple<int, int> fruitPosition;
//main map
bool map[size][size];


vector<tuple<int, int> > snakePosition;//current position, and position of body

clock_t lastTime=clock();
GLuint texid[3];                        //texture position


/**
 * @brief respawn fruit in random position
 *        been called only if snake had ate the fruit
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void spawnFruit(){
    get<0>(fruitPosition)=rand()%size;
    get<1>(fruitPosition)=rand()%size;
}


/**
 * @brief move the snake and update its body, also check if the head touch the body or not
 * @parameter void
 * @return void
 * @author Wenxuan
 * */
void updateMovement(){
    tuple<int,int> lastPosition=snakePosition[snakePosition.size()-1];//the last part of the body, if not eat the fruit, this will be deleted
    
    //loop through the body, update it to the next position
    for(unsigned i=1;i<snakePosition.size();i++){
        snakePosition[snakePosition.size()-i]= snakePosition[snakePosition.size()-i-1]; 
    }
    
    //move the head
    if(dirc==0){
        get<1>(snakePosition[0])=get<1>(snakePosition[0])+1;
    }else if(dirc==1){
        get<0>(snakePosition[0])=get<0>(snakePosition[0])-1;
    } else if(dirc==2){
        get<1>(snakePosition[0])=get<1>(snakePosition[0])-1;
    } else if(dirc==3){
        get<0>(snakePosition[0])=get<0>(snakePosition[0])+1;
    } 
    
    //check if the snake ate the fruit
    if(get<0>(snakePosition[0])==get<0>(fruitPosition)&& get<1>(snakePosition[0])==get<1>(fruitPosition)){
            snakeLength++;
            snakePosition.push_back(lastPosition);
            spawnFruit();
    }
    
    //check if the snake touch its body
     for(unsigned i=1;i<snakePosition.size();i++){
        if(get<0>(snakePosition[0])==get<0>(snakePosition[i])&& get<1>(snakePosition[0])==get<1>(snakePosition[i])){
                continu=0;
        }
    }
    
}

/**
 * @brief use as the keylistener for this game, can detect wasd and change direction of the smake
 * @param key: value of the key pressed
 *            int x, int y: unused, is needed in the keyboard function as to overwrite
 * @return void
 * @author Wenxuan
 * */
void keyboard(unsigned char key, int x, int y) {
    cout<<"key"<<endl;
    if(key=='w'&&dirc!=2){
        
        dirc=0;
        
    }
    if(key=='a'&&dirc!=3){
        
        dirc=1;
    }
    if(key=='s'&&dirc!=0){
        
        dirc = 2;
    }
    if(key=='d'&&dirc!=1){
        
        dirc = 3;
    }
    
    //update display
    glutPostRedisplay(); 
} 
    
    
    
/**
 * @brief use to display the game
 * @param void
 * @return void
 * @author Wenxuan
 * */
void display() {
    cout<<"display"<<endl;
    glClear(GL_COLOR_BUFFER_BIT);
    
    //began the gl
        glBegin(GL_QUADS);
        
        //draw the map first
        int colour=0;
        for(int x=0;x<size;x++){
            for(int y=0;y<size;y++){
                
                //change color each colume
                if(colour==0){
                    glColor3f(0.9f, 1.0f, 0.9f);  //light green
                    colour=1; 
                }else{
                    glColor3f(0.8f, 1.0f, 0.8f);  //darker green
                    colour=0;
                }
                
                //draw the square
                glVertex2f(x,y);     
                glVertex2f(x+1,y);   
                glVertex2f(x+1,y+1); 
                glVertex2f(x,y+1); 
                
            }
            
            //change the color each line
            if(colour==0){
                colour=1;
            }else{
                colour=0;
            }
        }
        glEnd();//end gl for draw the map
    
    
    
    //draw the snake, the head has one texture, which body is another texture
    bool head=true;
    for(tuple<int, int> snake : snakePosition){
        if(head){
            glBindTexture(GL_TEXTURE_2D, texid[0]); 
              head=false;
        }else{
            glBindTexture(GL_TEXTURE_2D, texid[1]); 
            }
             glBegin(GL_QUADS); 
        int x=get<0>(snake);
        int y=get<1>(snake);
            
        glTexCoord2i(x, y);glVertex2f(x,y);     
        glTexCoord2i(x+1, y);glVertex2f(x+1,y);   
        glTexCoord2i(x+1, y+1);glVertex2f(x+1, y+1); 
        glTexCoord2i(x, y+1);glVertex2f(x, y+1); 
        glEnd();
    }
    
    
    //draw the fruit
    glBindTexture(GL_TEXTURE_2D, texid[2]); 
    glBegin(GL_QUADS); 
    glTexCoord2i(get<0>(fruitPosition),get<1>(fruitPosition));glVertex2f(get<0>(fruitPosition),get<1>(fruitPosition));     
    glTexCoord2i(get<0>(fruitPosition)+1,get<1>(fruitPosition));glVertex2f(get<0>(fruitPosition)+1,get<1>(fruitPosition));   
    glTexCoord2i(get<0>(fruitPosition)+1,get<1>(fruitPosition)+1);glVertex2f(get<0>(fruitPosition)+1,get<1>(fruitPosition)+1); 
    glTexCoord2i(get<0>(fruitPosition),get<1>(fruitPosition)+1);glVertex2f(get<0>(fruitPosition),get<1>(fruitPosition)+1); 
    glEnd();
    
    //move if already pass some time after last move, this time will decrease when snake eat more fruit
    clock_t thisTime=clock();
    cout<<thisTime-lastTime<<endl;
    if(thisTime-lastTime>=300000-snakeLength*2000){
            lastTime=thisTime;
            updateMovement();
    }
    
    //check if game over or not
    if(get<0>(snakePosition[0])<0||get<1>(snakePosition[0])<0 || get<0>(snakePosition[0])>=size||get<1>(snakePosition[0])>=size){
            continu=0;
    }
    if(continu==0){
        cout<<"gameOver!"<<endl;
    exit(0);
    }
    glFlush();
    
    //loop
    glutPostRedisplay(); 
}
/**
 * @brief it load the image in the given filename using devil
 *      return -1 if error
 * @param char *filename filename to load from
 * @return int -1 if error
 * @author Wenxuan
 **/
int LoadImage(char *filename)
{
    ILboolean success; 
    ILuint image; 
    ilGenImages(1, &image); // Generation of one image name
    ilBindImage(image); //Binding of image name
    success = ilLoadImage(filename); // Loading of the image filename by DevIL
    if (success){
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); 
        if (!success){
            return -1;
        }
    }else{
        return -1;
    }
    return image;
}

/** 
 * @brief use to call LoadImage method, it will load the filename texture to the given index
 * @param string filename: the filename that this method will load, 
 *             int index: index of where will the texture be load to
 * @return void
 * @author Wenxuan
 **/
void autoLoad(string fileName, int index){
    
    /* load the file picture with DevIL */
    int  image = LoadImage(&fileName[0]);
    if ( image == -1 )
    {
        printf("Can't load picture file by DevIL ");
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
       glGenTextures(3, &texid[index]); /* Texture name generation */
       glBindTexture(GL_TEXTURE_2D, texid[index]); /* Binding of texture name */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
       glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 
        0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */
    
    
}

/** 
 * @brief clear the board on init gl
 *        also set the vertex and viewport for gl
 * @param void
 * @return void
 * @author Wenxuan
 **/
void initGL(){
     glViewport(0, 0, 500, 500);              //use a screen size of WIDTH x HEIGHT
     glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
     gluOrtho2D(0,size,0,size);                                   //set the sceen coordinate from 0 to 50
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
    }
    
/** 
 * @brief use to init the setting the game and run the mainloop 
 * @param int, char **
 * @return int
 * @author Wenxuan
 **/
int main (int argc, char** argv) {
    //use time as random seed 
    srand(time(NULL));
    snakePosition.push_back(tuple<int, int>(size/2, size/2));
    spawnFruit();
    //make the game window
    int argc1 = 1;
    char *argv1[1]={(char*)"snake"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Games"); 
    //add displayfunction, keyboard listener, and init the gl
    glutDisplayFunc(display); 
    glutKeyboardFunc(keyboard); 
    initGL();
    //reshape window, start the game
    int maxSize=size;
    glutReshapeWindow((500*size)/maxSize,(500*size)/maxSize);
    cout<<"dd"<<endl;
    //check if the user have the correct DevIL version
     if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
     {
           printf("wrong DevIL version ");
           return -1;
     }
     
    //init the DevIL, load the texture
    ilInit(); 
    autoLoad("head.jpg",0);
    autoLoad("body.jpg",1);
    autoLoad("fruit.jpg",2);
    glutMainLoop();
}
