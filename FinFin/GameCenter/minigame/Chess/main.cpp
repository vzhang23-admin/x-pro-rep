

/**
 * main.cpp
 * @brief main class for a chess game 
 *        use opengl and il for game window and read in image
 *        use ChessClass as type store each chess piece's value
 * @author YeBei Gong
 */




#include <GL/glut.h> 
#include <IL/il.h>
#include "ChessClass.h"

using namespace std;                   //use std as namespace, so no need to add std:: in front of string, tuple, and vector
ChessClass chessBoard[8][8];            //main board the game take place in
const int length=8, width=8;            //length and width, in chess, it is set to 8, const
int win=-1;                             //if the win is 0, white win, 1 is black win
int thisTurn=-1;                        //0 is white's turn, 1 is black's turn
int wantToMoveX=4, wantToMoveY=4;       //record which piece did the mouse select
vector<tuple<int, int> > availableMove; //store the possable moves of the selected piece 
int mousex, mousey;                     //store mousex and mousey when mouse pressed
int sizeWindow=400;                     //set size of the window to be 400
GLuint texid[12];                       //there are 12 image store as texture




/**
 * 
 * @brief display function
 *        use to display the graphic for Chess
 *        draw the board and all the pieces 
 * @returns void
 * @author Yebei Gong
 * 
 */

void display() {
        
        //if checked someone win, end the game
        if(win!=-1){
            exit(0);
            }
            
        //clear the screen
        glClear(GL_COLOR_BUFFER_BIT);
        
        
        //began the gl
        glBegin(GL_QUADS);
        
        //draw the board first, with colour white and black
        int colour=0;
        for(int x=0;x<8;x++){
            for(int y=0;y<8;y++){
                
                //change color each colume
                if(colour==0){
                    glColor3f(1.0f, 1.0f, 1.0f);//white color
                    colour=1;
                }else{
                    glColor3f(0.5f, 0.5f, 0.5f);   //black color(more like gray, since complete black did not look good)
                    colour=0;
                }
                
                //draw the square, since the screen size is set to max of 8, +1 will point to the coor of the next grid
                glVertex2f(x,y);     
                glVertex2f(x+1,y);   
                glVertex2f(x+1,y+1); 
                glVertex2f(x,y+1); 
                
            }
            
            //change the color each line
            if(colour==0){
                colour=1;
            }else{
                colour=0;
            }
        }
        glEnd();//end gl for draw the board
        
        
        //draw each piece
        for(int x=0;x<8;x++){
            for(int y=0;y<8;y++){
                
                //if is not empty, start draw
                if(chessBoard[x][y].getType()!="empty"){
                    
                    //textureID will represent which texture we want to use
                    int textureID=0;
                    
                    //assign the texture id to correct piece
                    if(chessBoard[x][y].getType()=="queen"){
                        textureID+=1;
                    }else if(chessBoard[x][y].getType()=="rook"){
                        textureID+=2;
                    }else if(chessBoard[x][y].getType()=="bishop"){
                        textureID+=3;
                    }else if(chessBoard[x][y].getType()=="knight"){
                        textureID+=4;
                    }else if(chessBoard[x][y].getType()=="pawn"){
                        textureID+=5;
                    }
                    if(chessBoard[x][y].getColour()==1){
                        textureID+=6;
                    }
                    
                    //change the texture, and start draw
                    glBindTexture(GL_TEXTURE_2D, texid[textureID]); 
                    glBegin(GL_QUADS);
                    glTexCoord2i(x, y); glVertex2i(x,y);
                    glTexCoord2i(x, y+1); glVertex2i(x,y+1);
                    glTexCoord2i(x+1, y+1); glVertex2i(x+1,y+1);
                    glTexCoord2i(x+1, y); glVertex2i(x+1,y);
                    
                    glEnd();
                    glBindTexture(GL_TEXTURE_2D, 0);//set texture to 0(no texture)
                } 
            }
        }
        
        //draw the available move
        glBegin(GL_QUADS);
        glColor3f(1.0f, 0.0f, 0.0f);//set color to red
        
        //draw a red dot at each possable move
        for(tuple<int, int> move : availableMove){
                double vertx=get<0>(move)+0.4f;
                double verty=get<1>(move)+0.4f;
                glVertex2f(vertx,verty);     
                glVertex2f(vertx+0.2,verty);   
                glVertex2f(vertx+0.2,verty+0.2); 
                glVertex2f(vertx,verty+0.2); 
        }
        glEnd();//end
        
        //flush the display
        glFlush();
}

/**
 * 
 * @brief Start GL by setting the width x height
 * @param void
 * @returns void
 * @author Yebei Gong
 * 
 */
void initGL(){
     glViewport(0, 0, sizeWindow, sizeWindow);              //use a screen size of WIDTH x HEIGHT
     glEnable(GL_TEXTURE_2D);                               //enable 2D texturing
     gluOrtho2D(0,8,0,8);                                   //set the sceen coordinate from 0 to 8
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    //clear the window
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);                  //clear the color  
    }



/**
 * 
 * @brief set all the pieces on to the board in the right position
 * @param void
 * @returns void
 * @author Yebei Gong
 * 
 */
void initBoard(){
        
        //set the type and color of each piece
        for(int i=0;i<8;i++){
            chessBoard[i][0].setColour(0);
            chessBoard[i][1].setColour(0);
            chessBoard[i][1].setType("pawn");
            chessBoard[i][7].setColour(1);
            chessBoard[i][6].setColour(1);
            chessBoard[i][6].setType("pawn");
        }
       
        chessBoard[0][0].setType("rook");
        chessBoard[7][0].setType("rook");
        chessBoard[1][0].setType("knight");
        chessBoard[6][0].setType("knight");
        chessBoard[2][0].setType("bishop");
        chessBoard[5][0].setType("bishop");
        chessBoard[4][0].setType("king");
        chessBoard[0][7].setType("rook");
        chessBoard[7][7].setType("rook");
        chessBoard[1][7].setType("knight");
        chessBoard[6][7].setType("knight");
        chessBoard[2][7].setType("bishop");
        chessBoard[5][7].setType("bishop");
        chessBoard[4][7].setType("king");
        chessBoard[3][0].setType("queen");
        chessBoard[3][7].setType("queen");
        
        //set the turn to be white's turn, since white start first every time
        thisTurn=0;
        
}

/**
 * 
 * @brief function to move the pieces into the position you want 
 *        it has to follow the chess rules        
 * @param int 
 *        original position of: x, y
 *        the position you want to move it to: goalX, goalY
 * @returns void
 * @author Yebei Gong
 * 
 */


void movePiece(int x, int y, int goalX, int goalY){
    
    //if it eat a king, winner is the color of the piece who eat the king
    if(chessBoard[goalX][goalY].getType()=="king"){
        win=chessBoard[x][y].getColour();
        
        }
        
    //move the piece by copy the piece to the goal and make a empty spot in the original position
    chessBoard[goalX][goalY]=chessBoard[x][y];
    chessBoard[x][y]=ChessClass();
    
    //if pawn move to the end of the board, make it in to a queen
    if((goalY==0||goalY==7)&&chessBoard[goalX][goalY].getType()=="pawn"){
        chessBoard[goalX][goalY].setType("queen");
        }
    return;
}

/**
 * 
 * @brief vector to get the possable move of a given  in the x and y position
 *        it will check the surrounding to give all the legal moves following chess rules        
 * @param  vector getMove
 * @return moves
 * @author Yebei Gong
 * 
 */

vector<tuple<int, int> >  getMove( int x, int y){
	
	vector<tuple<int, int> >  moves;
    int colour=chessBoard[x][y].getColour();
    string type=chessBoard[x][y].getType();
    
    //get move for king type
	if(type=="king"){
        //loop from -1 to 1 difference for x and y position
		for(int i=-1;i<2;i++){
				for(int e=-1;e<2;e++){
						if(i+x>=0&&e+y>=0&&i+x<8&&e+y<8){
							if(chessBoard[i+x][e+y].getType()=="empty"||chessBoard[i+x][e+y].getColour()!=colour){  //if nothing in the goal, add it to the possable move vector
								moves.push_back(tuple<int, int>(i+x, e+y));
							}
						}
				}
		}
	}else if(type=="queen"){//get move for queen type
        
        //get the move in x+ position
		for(int i=1;i<8;i++){
            if(i+x<8){
                //if the position is empty, add it to the available move
                if(chessBoard[x+i][y].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y));
                }else{
                    //else,the piece blocking the move is different colour, add position of the piece to the vector
                    if(chessBoard[x+i][y].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y));
                    }
                    
                    //break, since some piece is blocking the move
                    break;
                }
            }
		}
        
        //get the move in x- position
        for(int i=-1;i>-8;i--){
            if(i+x>=0){
                if(chessBoard[x+i][y].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y));
                }else{
                    if(chessBoard[x+i][y].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y));
                    }   
                    break;
                }
            }
		}
        
        //get the move in y+ position
        for(int i=1;i<8;i++){
            if(i+y<8){
                    if(chessBoard[x][y+i].getType()=="empty"){
                        moves.push_back(tuple<int, int>(x, y+i));
                    }else{
                        if(chessBoard[x][y+i].getColour()!=colour) {
                            moves.push_back(tuple<int, int>(x, y+i));
                        }   
                        break;
                         
                    }
                }
		}
        
        //get the move in y- position
        for(int i=-1;i>-8;i--){
            if(i+y>=0){
                if(chessBoard[x][y+i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x, y+i));
                }else{
                    if(chessBoard[x][y+i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x, y+i));
                    }   
                     break;
                }
            }
		}
        
        //get the move in x+ y+ position
        for(int i=1;i<8;i++){
            if(i+y<8&&i+x<8){
                    if(chessBoard[x+i][y+i].getType()=="empty"){
                        moves.push_back(tuple<int, int>(x+i, y+i));
                    }else{
                        if(chessBoard[x+i][y+i].getColour()!=colour) {
                            moves.push_back(tuple<int, int>(x+i, y+i));
                        } 
                        break; 
                    }
            }
		}
        //get the move in x- y- position
        for(int i=-1;i>-8;i--){
            if(i+y>=0&&i+x>=0){
                if(chessBoard[x+i][y+i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y+i));
                }else{
                    if(chessBoard[x+i][y+i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y+i));
                    }
                    break;
                }
            }
		}
        
        //get the move in x+ y- position
        for(int i=1;i<8;i++){
            if(y-i<8&&i+x<8){
                if(chessBoard[x+i][y-i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y-i));
                }else{
                    if(chessBoard[x+i][y-i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y-i));
                    }   
                    break;
                }
            }
		}
        //get the move in x- y+ position
        for(int i=-1;i>-8;i--){
            if(y-i>=0&&i+x>=0){
                if(chessBoard[x+i][y-i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y-i));
                }else{
                    if(chessBoard[x+i][y-i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y-i));
                    }   
                    break;
                }
            }
		}
        
    //get the black pawn move
	}else if(type=="pawn"&&colour==1){
        
        //add front move if no piece blocking it, can move two block at the start
		if(chessBoard[x][y-1].getType()=="empty"){
			moves.push_back(tuple<int, int>(x, y-1));
            if(y==6&&chessBoard[x][y-2].getType()=="empty"){
                moves.push_back(tuple<int, int>(x, y-2));
            }
        }
        
        //add eat piece move
 		if(y>0&&x<7&&chessBoard[x+1][y-1].getType()!="empty"&&chessBoard[x+1][y-1].getColour()!=colour)
			moves.push_back(tuple<int, int>(x+1, y-1));
		
		if(x>0&&y>0&&chessBoard[x-1][y-1].getType()!="empty"&&chessBoard[x-1][y-1].getColour()!=colour)
			moves.push_back(tuple<int, int>(x-1, y-1));
            
    //get the white pawn move
	}else if(type=="pawn"){
        
        //almost same as black pawn
		if(chessBoard[x][y+1].getType()=="empty"){
			moves.push_back(tuple<int, int>(x, y+1));
            if(y==1&&chessBoard[x][y+2].getType()=="empty"){
                moves.push_back(tuple<int, int>(x, y+2));
            }
        }
			
		if(x<7&&y<7&&chessBoard[x+1][y+1].getType()!="empty"&&chessBoard[x+1][y+1].getColour()!=colour)
			moves.push_back(tuple<int, int>(x+1, y+1));
		if(x>0&&y<7&&chessBoard[x-1][y+1].getType()!="empty"&&chessBoard[x-1][y+1].getColour()!=colour)
			moves.push_back(tuple<int, int>(x-1, y+1));

	}else if(type=="rook"){
        
        //same as the first part of the queen movement
		 //get the move in x+ position
		for(int i=1;i<8;i++){
            if(i+x<8){
                //if the position is empty, add it to the available move
                if(chessBoard[x+i][y].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y));
                }else{
                    //else,the piece blocking the move is different colour, add position of the piece to the vector
                    if(chessBoard[x+i][y].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y));
                    }
                    
                    //break, since some piece is blocking the move
                    break;
                }
            }
		}
        
        //get the move in x- position
        for(int i=-1;i>-8;i--){
            if(i+x>=0){
                if(chessBoard[x+i][y].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y));
                }else{
                    if(chessBoard[x+i][y].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y));
                    }   
                    break;
                }
            }
		}
        
        //get the move in y+ position
        for(int i=1;i<8;i++){
            if(i+y<8){
                    if(chessBoard[x][y+i].getType()=="empty"){
                        moves.push_back(tuple<int, int>(x, y+i));
                    }else{
                        if(chessBoard[x][y+i].getColour()!=colour) {
                            moves.push_back(tuple<int, int>(x, y+i));
                        }   
                        break;
                         
                    }
                }
		}
        
        //get the move in y- position
        for(int i=-1;i>-8;i--){
            if(i+y>=0){
                if(chessBoard[x][y+i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x, y+i));
                }else{
                    if(chessBoard[x][y+i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x, y+i));
                    }   
                     break;
                }
            }
		}
	}else if(type=="bishop"){
        
        //get the move in x+ y+ position
        for(int i=1;i<8;i++){
            if(i+y<8&&i+x<8){
                    if(chessBoard[x+i][y+i].getType()=="empty"){
                        moves.push_back(tuple<int, int>(x+i, y+i));
                    }else{
                        if(chessBoard[x+i][y+i].getColour()!=colour) {
                            moves.push_back(tuple<int, int>(x+i, y+i));
                        } 
                        break; 
                    }
            }
		}
        //get the move in x- y- position
        for(int i=-1;i>-8;i--){
            if(i+y>=0&&i+x>=0){
                if(chessBoard[x+i][y+i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y+i));
                }else{
                    if(chessBoard[x+i][y+i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y+i));
                    }
                    break;
                }
            }
		}
        
        //get the move in x+ y- position
        for(int i=1;i<8;i++){
            if(y-i<8&&i+x<8){
                if(chessBoard[x+i][y-i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y-i));
                }else{
                    if(chessBoard[x+i][y-i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y-i));
                    }   
                    break;
                }
            }
		}
        //get the move in x- y+ position
        for(int i=-1;i>-8;i--){
            if(y-i>=0&&i+x>=0){
                if(chessBoard[x+i][y-i].getType()=="empty"){
                    moves.push_back(tuple<int, int>(x+i, y-i));
                }else{
                    if(chessBoard[x+i][y-i].getColour()!=colour) {
                        moves.push_back(tuple<int, int>(x+i, y-i));
                    }   
                    break;
                }
            }
		}
    }else{
        
        //get move for knight class
        int cx=x+1;
        int cy=y+2;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
         cx=x+2;
         cy=y+1;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
         cx=x-1;
         cy=y+2;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
         cx=x-2;
         cy=y+1;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
         cx=x+1;
         cy=y-2;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
         cx=x+2;
         cy=y-1;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
        cx=x-1;
        cy=y-2;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
        cx=x-2;
         cy=y-1;
        if(cx<8&&cy<8&&cx>=0&&cx>=0&&chessBoard[cx][cy].getColour()!=colour){
                moves.push_back(tuple<int, int>(cx, cy));
        }
        
        
    }
	return moves;
}




/**
 * 
 * @brief   doClick method
 *          called in the mouse listener
 *          get the piece user select, and get the available move of it
 *          if available move is not empty, and click on the available move, 
 *          it will move the piece to the position clicked        
 * @param  void
 * @return void
 * @author Yebei Gong
 * 
 */
void doClick(){
    int realx=8*mousex/sizeWindow;
    int realy=8*(sizeWindow-mousey)/sizeWindow;
    for(tuple<int, int> move : availableMove){
        if(get<0>(move)==realx&&get<1>(move)==realy){
            movePiece(wantToMoveX, wantToMoveY, realx, realy);
            if (thisTurn==1){
                thisTurn=0;
            }else{
                thisTurn=1;
            }
            availableMove=vector<tuple<int, int> >();
            return;
        }
    }
    if(chessBoard[realx][realy].getColour()==thisTurn){
        wantToMoveX=realx;
        wantToMoveY=realy;
        availableMove=getMove(realx, realy);
    }
}


/**
 * 
 * @brief   myMouseFunc
 *          the mouse listener
 *          call mouseClick method when mouse click    
 * @param  int button, state, x, y of mouse
 * @return void
 * @author Yebei Gong
 * 
 */
void myMouseFunc(int button, int state, int x, int y){ 
	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		mousex=x;
        mousey=y;
		doClick();
	}
}

/**
 * 
 * @brief   reshape function, to make sure user will not reshape the window  
 * @param  void
 * @return void
 * @author Yebei Gong
 * 
 */

void reshape(GLsizei newwidth, GLsizei newheight) 
{  
    glutReshapeWindow(sizeWindow,sizeWindow);
}

/**
 * 
 * @brief   LoadImage method
 *          return -1 if error
 *          Load an image using DevIL
 * @param  char* filename
 * @return image
 * @author Wenxuan Zhang
 * 
 */
int LoadImage(char *filename)
{
    ILboolean success; 
    ILuint image; 
    ilGenImages(1, &image); // Generation of one image name
    ilBindImage(image); //Binding of image name
    success = ilLoadImage(filename); // Loading of the image filename by DevIL
    if (success){
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); 
        if (!success){
            return -1;
        }
    }else{
        return -1;
    }
    return image;
}


/**
 * @brief use to call LoadImage method, it will load the filename texture to the given index
 * @param string filename: the filename that this method will load, 
 *             int index: index of where will the texture be load to
 * @return void
 * @author Wenxuan
 * */
 
void autoLoad(string fileName, int index){
    
    /* load the file picture with DevIL */
    int  image = LoadImage(&fileName[0]);
    if ( image == -1 )
    {
        printf("Can't load picture file by DevIL ");
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
       glGenTextures(12, &texid[index]); /* Texture name generation */
       glBindTexture(GL_TEXTURE_2D, texid[index]); /* Binding of texture name */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
       glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 
        0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */
    
    
}


/** 
 * @brief use to init the setting the game and run the mainloop 
 * @param int, char **
 * @return int
 * @author Yebei Gong
 * */

int main (int argc, char** argv) {
    //init the chessBoard
    for(int i=0;i<8;i++){
            for(int e=0;e<8;e++){
                chessBoard[i][e]=ChessClass();
            }
    }
    
    //make window
    int argc1 = 1;
    char *argv1[1]={(char*)"Chess"};
    glutInit(&argc1,argv1);
    glutCreateWindow("Game");  
    
    //add listener, and init the GL and the board
    glutMouseFunc(myMouseFunc);
    glutDisplayFunc(display); 
    initGL();
    initBoard();
    glutReshapeWindow(sizeWindow,sizeWindow);
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    
    
    
    //check if the user have the correct DevIL version
     if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
     {
           printf("wrong DevIL version ");
           return -1;
     }
     
    //init the DevIL, liad the texture
    ilInit(); 
    autoLoad("whiteKing.jpg",0);
    autoLoad("whiteQueen.jpg",1);
    autoLoad("whiteRook.jpg",2);
    autoLoad("whiteBishop.jpg",3);
    autoLoad("whiteKnight.jpg",4);
    autoLoad("whitePawn.jpg",5);
    autoLoad("blackKing.jpg",6);
    autoLoad("blackQueen.jpg",7);
    autoLoad("blackRook.jpg",8);
    autoLoad("blackBishop.jpg",9);
    autoLoad("blackKnight.jpg",10);
    autoLoad("blackPawn.jpg",11);
    
    
    //set the texture to 0
    glBindTexture(GL_TEXTURE_2D, texid[0]);
    
    
    /* Main loop */
    glutMainLoop();
    
    /* Delete used resources and quit */
     glDeleteTextures(12, texid);
}
