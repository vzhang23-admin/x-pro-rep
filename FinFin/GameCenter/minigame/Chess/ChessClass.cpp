/**
 * 
 * @brief   ChessClass.cpp
 * 			class file for chess
 * @author Yebei Gong
 * 
 */


#include "ChessClass.h"
using namespace std;

/**
 * 
 * @brief Constructor for chess
 * @author Yebei Gong
 * 
 */
ChessClass::ChessClass(){
	type="empty";
	colour=-1;
}

/**
 * 
 * @brief  getter for colour
 * @author Yebei Gong
 * 
 */
int ChessClass::getColour(){
	return colour;
}

/**
 * 
 * @brief  getter for chess piece 
 * @author Yebei Gong
 * 
 */
string ChessClass::getType(){
	return type;
}

/**
 * 
 * @brief  setter for colour
 * @param int colour 
 * @author Yebei Gong
 * 
 */
void ChessClass::setColour(int cc){
	colour=cc;
}

/**
 * 
 * @brief  setter for type
 * @param string type
 * @author Yebei Gong
 * 
 */
void ChessClass::setType(string ct){
	type=ct;
}



