#include "mainwindow.h"
/**
 * @brief qMain will construct the mainWindow and execute it to open
 * @param argc
 * @param argv
 * @return executes the application
 * @author Dan Lous
 */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setStyle("fusion");
    MainWindow mainWindow;
    return app.exec();
}
