#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;


/**
 * @brief MainWindow::MainWindow
 *        main body for making the GUI
 *        create buttons based on the games in the game folders
 *        then positions the game center logo, game library, descriptions and game image
 *        contains event calling for buttons and and formatting each layout
 *        at the end, it will display the game center window to the user
 * @param parent
 * @return nothing
 * @author Dan Lou
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
     
{
    //layout for gamecenter
    widget = new QWidget();
    widget->setWindowTitle("Game Center");
    gameCenter = new QGridLayout();
    formLayout = new QFormLayout();
    imageLayout=new QGridLayout();
    groupBox = new QGroupBox("Game Library");

    //run shell script to get all game and add save file if did not have one
    system("./getFolderName.sh");
    system("./addSaveFile.sh");
    
    //read in the name of all games
    FILE *fp=fopen("name.txt","r");
    int count=0;
    int c;
    
    //see how many games are there
    for (c = getc(fp); c != EOF; c = getc(fp)){ //if '\n' detect, add one to count
        	
            if (c == '\n'){ 
            		count = count + 1;
		} 
	}
     
     
    //init the game list 
    list=new QPushButton*[count];
    
    fseek(fp, 0, SEEK_SET);
    
    
    //for everygame, make a button, and put a image beside the text
    int count2=0;
    char line[100];
    QPixmap pixmap("button.jpg");
    QIcon ButtonIcon(pixmap);
    
    while(fscanf(fp, "%100s", line) == 1){
        line[strlen(line)-1]='\0';
        list[count2]=new QPushButton(QObject::tr(line));
        list[count2]->setIcon(ButtonIcon);
        count2++;
    }
    
    //record the name of the first game select(use in startgame function to start the game)
    strcpy(nameGame,(char *)(list[0]->text().toStdString().c_str()));
    fseek(fp, 0, SEEK_SET);
    
    
    //find the game description, savefile, and game picture for the first game
    char firstGamePath[100];
    fscanf(fp, "%100s", firstGamePath);
    char pathDes[1024];
    sprintf(pathDes, "./minigame/%sdescription.txt", firstGamePath);
    char pathPic[1024];
    sprintf(pathPic, "./minigame/%spicture.jpg", firstGamePath);
    cout<<"D"<<endl;
    char pathPlay[1024];
    sprintf(pathPlay, "./minigame/%ssave.txt", firstGamePath);
    FILE *readPlayed=fopen(pathPlay,"r");
    
    char played[100];
    
    //see how many times user played this game
    if(fscanf(readPlayed, "%100s", played)!=1){
        
            played[0]='0';
            played[1]='\0';
    }
    char playTime[1024];
    sprintf(playTime, "\n You have played this game for %s times *", played);
    fclose(readPlayed);
    
    
    // Read In Game Description
    FILE *description=fopen(pathDes,"r");
    char des[10000];
    int count3=0;
    for (c = getc(description); c != EOF; c = getc(description)){ //if '\n' detect, add one to count
            des[count3]=c;
            count3++;
	}
    //put the played time at the end of game description
    int countTimes=0;
    while(playTime[countTimes]!='*'){
        des[count3+countTimes]=playTime[countTimes];
        countTimes++;
    }
    des[count3+countTimes]='\0';
    desc = new QTextEdit();
    desc->setText(des);;
    desc->setReadOnly(true);
    
    
    //read in logo for x-pro
    QImage logo = *new QImage("logo.png");
    QLabel *logoLabel=new QLabel("");
    logoLabel->setPixmap(QPixmap::fromImage(logo));
    
    // Game Image
    QImage inputImg = *new QImage(pathPic);
    gameImg = new QLabel("");
    inputImg=inputImg.scaled(500, 300, Qt::KeepAspectRatio);
    gameImg->setPixmap(QPixmap::fromImage(inputImg));
    gameImg->setAlignment(Qt::AlignRight);
    
    
    //add the button to layout
    for(int i=0;i<count;i++){
         formLayout->addRow(list[i]);
    }

    // Making the game lilbrary scrollable
    groupBox->setLayout(formLayout);
    scroll = new QScrollArea();
    
    //add function to button
    for(int i=0;i<count;i++){
        connect(list[i], SIGNAL(clicked()), this, SLOT(buttonPressed()));
        
    }
    
    //add everything to the layout
    scroll->setWidget(groupBox);
    scroll->setWidgetResizable(true);
    scroll->setFixedHeight(290);
    scroll->setFixedWidth(600);
    desc->setFixedHeight(300);
    gameCenter->addWidget(logoLabel, 0, 0, 1, 1);
    gameCenter->addWidget(scroll, 1, 0, 3, 1);
    gameCenter->addWidget(desc, 0, 1, 1, 1);
    gameCenter->addWidget(gameImg, 2, 1, 1, 1);
    QPushButton *button=new QPushButton(tr("start game"));
    connect(button, SIGNAL(clicked()), this, SLOT(startGame()));
    gameCenter->addWidget(button, 3, 1, 1, 1);
    widget->setLayout(gameCenter);
    widget->setFixedSize(QSize(1150, 650));
    widget->show();
    fclose(fp);
    fclose(description);
}

/**
 * @brief MainWindow::~MainWindow
 *        Deconstructor for the MainWindow
 *
 * @return nothing
 * @author Dan Lou
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::startGame
 *        function used to start the game within the minigame folder
 *        the game started will be "nameGame"
 *
 * @return nothing
 * @author Dan Lou
 */
void MainWindow::startGame()
{
    
    char path[1024];
    char pathPlay[150];
    
    
    //update the save.txt to add one to the count
    sprintf(pathPlay, "./minigame/%s/save.txt", nameGame);
    FILE *readPlayed=fopen(pathPlay,"r");
    char played[100];
    if(fscanf(readPlayed, "%100s", played)!=1){
        
            played[0]='0';
            played[1]='\0';
    }
    widget->hide();
    fclose(readPlayed);
    int number=atoi(played);
    number+=1;
    char shellCommand[200];
    sprintf(shellCommand, "echo %d > '%s'", number, pathPlay);
    system(shellCommand);
    
    //put unix commant to start the game in the path variable and run the game
    sprintf(path, "cd minigame/%s ; ./main", nameGame);
    
    system(path);
    
    // read the Game Description
    char playTime[100];
    sprintf(playTime,"\n You have played this game for %d times *", number);
    char pathDes[1024];
    sprintf(pathDes, "./minigame/%s/description.txt", nameGame);
    FILE *readPointer=fopen(pathDes,"r");
    char doc[10000];
    int count4=0;
    for (int c = getc(readPointer); c != EOF; c = getc(readPointer)){ 
            doc[count4]=c;
            count4++;
	}
    int countTimes=0;
    while(playTime[countTimes]!='*'){
        doc[count4+countTimes]=playTime[countTimes];
        countTimes++;
    }
    doc[count4+countTimes]='\0';
    
    //update game description
    desc->setText(doc);
    fclose(readPointer);
    widget->show();
}

/**
 * @brief MainWindow::buttonPressed
 *        event function where on the button clicked
 *        it will change the description of the game selected
 *        as well change the image of the game selected
 *
 * @image the image will be located based on folder path, and will get the image
 *        for the game selected on the button clicked
 *
 * @return nothing
 * @author Dan Lou
 */
void MainWindow::buttonPressed()
{
    //get which button the user pressed
    auto button = qobject_cast<QPushButton *>(sender());
    Q_ASSERT(button);
    int c;
    
    //get the name of the game, update the description, save image
    char * name=(char *)(button->text().toStdString().c_str());
    strcpy(nameGame, name);
    char pathDes[1024];
    sprintf(pathDes, "./minigame/%s/description.txt", name);
    char pathPic[1024];
    sprintf(pathPic, "./minigame/%s/picture.jpg", name);
    char pathPlay[1024];
    sprintf(pathPlay, "./minigame/%s/save.txt", name);
    FILE *readPlayed=fopen(pathPlay,"r");
    
    char played[100];
    
    if(fscanf(readPlayed, "%100s", played)!=1){
        
            played[0]='0';
            played[1]='\0';
    }
    char playTime[1024];
    sprintf(playTime, "\n\n You have played this game for %s times *", played);
    fclose(readPlayed);
    
    
    
    // Game Description
    FILE *readPointer=fopen(pathDes,"r");
    char doc[10000];
    int count4=0;
    for (c = getc(readPointer); c != EOF; c = getc(readPointer)){ 
            doc[count4]=c;
            count4++;
	}
    
    //add time pleyed to the of the game description
    int countTimes=0;
    while(playTime[countTimes]!='*'){
        doc[count4+countTimes]=playTime[countTimes];
        countTimes++;
    }
    doc[count4+countTimes]='\0';
    
    //update the display
    desc->setText(doc);
    QImage inputImg = *new QImage(pathPic);
    
    inputImg=inputImg.scaled(500, 300, Qt::KeepAspectRatio);
    gameImg->setPixmap(QPixmap::fromImage(inputImg));
    fclose(readPointer);
    
}
