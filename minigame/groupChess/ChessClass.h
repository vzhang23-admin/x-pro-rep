

/**
 * 
 * @brief   ChessClass.h
 * 			header for ChessClass
 * 			store information about chess pieces
 * @param  filename
 * @return image
 * @author Wenxuan Zhang
 * 
 */


#include <iostream>
#include <vector>
#include <tuple>
#ifndef CHESSCLASS_H
#define CHESSCLASS_H


class ChessClass{
	private:
		std::string type;				//type of the piece
		int colour;						//color of the piece
	public:
		ChessClass ();					//constructor
		
		//setter and getter
		void setColour(int cc);			
		void setType(std::string ct);
		std::string getType();
		int getColour();
		
	
};
#endif
