#include <iostream>
using namespace std;
#include <unistd.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <cstring>

string gameName; //the game you want to run
string gameFolder; 

char command [100];
char game [100];
char s[100];
bool pass = true; 


void deleteGame(){
    cout << "Which game do you want to delete?" << endl;
    cin >> gameName;
    strcpy(command, gameName.c_str());
    chdir ("minigame");
    chdir(command);
    printf("%s\n", getcwd(s, 100)); 
    int x = execlp("rm","rm", "main", NULL);


} 

void runGame(){


    /*cout<< "Run which game: ";
    cin >> gameName;
    strcpy(game, gameName.c_str());*/

    //if game already exist, don't need get user input

    
    gameName = "./minigame/" + gameName + "/main";//./game2/game
    strcpy(command, gameName.c_str());
    int x = execlp(command,command,NULL); //execute program
        if (x==-1){
            cout << "This game cannot run, check if it has been added" << endl;
        }
    
}

void addGame(){
        cout<< "What is the name of the new game: ";
        cin >> gameName;
        strcpy(game, gameName.c_str());
        //printf("%s\n", getcwd(s, 100)); 
        //change dir to the new game added
        chdir ("minigame");
        chdir(game);
        //printf("%s\n", getcwd(s, 100)); 

        int x = execlp("./RUNME.sh","./RUNME.sh",NULL);
        if (x==-1){
            cout << "This game has not been added in the minigame folder, please add the game in" << endl;
            pass = false;
        }
}

int main() {
    int input; 

    string test;
    cout<< "1. Add new game" << endl;
    cout<< "2. Run new game" << endl;
    cout<< "3. Delete a game" <<endl;
    
    cin >> input;

    if (input ==1){
        addGame();
        cout << "Added game" << endl;
    
    }
    if (input == 2){
        
        runGame();
    }
    if (input == 3){
        deleteGame();
    }
      

    
    return 0;
}

