#include <iostream>
using namespace std;
#include <unistd.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <cstring>


//Make sure the game you want to run is compiled first 
//compile the ./runme.sh for the game first

void runGame(string gameName){
    char command [100];

    gameName = "./minigame/" + gameName + "/main";//./game2/game
    strcpy(command, gameName.c_str());
    int x = execlp(command,command,NULL); //execute program
        if (x==-1){
            cout << "This game cannot run, check if it has been added" << endl;
        }
    
}