#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QImage>
#include <QFormLayout>
#include <QGroupBox>
#include <QScrollArea>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void buttonPressed();
private:
    Ui::MainWindow *ui;
    QPushButton *chess;
    QPushButton *maze;
    QPushButton *snake;
    QPushButton *sudoku;
    QWidget *widget;
    QGridLayout *gameCenter;
    QLabel *desc;
    QImage *input_image;
    QFormLayout *formLayout;
    QGroupBox *groupBox;
    QScrollArea *scroll;
};
#endif // MAINWINDOW_H
