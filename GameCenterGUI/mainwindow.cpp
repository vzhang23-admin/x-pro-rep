#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCoreApplication>
#include <QtWidgets>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    widget = new QWidget();
    widget->setWindowTitle("Game Center");
    gameCenter = new QGridLayout();
    formLayout = new QFormLayout();
    groupBox = new QGroupBox("Game Library");

    // Game Description
    desc = new QLabel("Game Description gnoieqrngoeqngoqriowegiergoqeirgnoieqrngoeqngoq Game Description weijriowegiergoqeirgnoieqrngoeqngoq Game Description weijriowegiergoqeirgnoieqrngoeqngoq Game Description weijriowegiergoqeirgnoieqrngoeqngoq");
    desc->setWordWrap(true);
    // Game Image
    input_image = new QImage("C:/Users/danie/OneDrive/Desktop/University/Thid year/CS 3357/Assignment 2/Assignment 2/blackhole.png");
    QLabel *gameImg = new QLabel("");
    gameImg->setPixmap(QPixmap::fromImage(*input_image));

    chess = new QPushButton();
    chess->setText("chess");
    maze = new QPushButton();
    maze->setText("maze");
    snake = new QPushButton();
    snake->setText("snake");
    sudoku = new QPushButton();
    sudoku->setText("sudoku");

    formLayout->addRow(chess);
    formLayout->addRow(maze);
    formLayout->addRow(snake);
    formLayout->addRow(sudoku);

    // Making the game lilbrary scrollable
    groupBox->setLayout(formLayout);
    scroll = new QScrollArea();

    connect(chess, SIGNAL(clicked()), this, SLOT(buttonPressed()));
    connect(maze, SIGNAL(clicked()), this, SLOT(buttonPressed()));
    connect(snake, SIGNAL(clicked()), this, SLOT(buttonPressed()));
    connect(sudoku, SIGNAL(clicked()), this, SLOT(buttonPressed()));
    scroll->setWidget(groupBox);
    scroll->setWidgetResizable(TRUE);
    scroll->setFixedHeight(600);
    scroll->setFixedWidth(600);

    gameCenter->addWidget(scroll, 0, 0, -1, 1);
    gameCenter->addWidget(desc, 0, 1, 1, 1);
    gameCenter->addWidget(gameImg, 1, 1, 1, 1);

    widget->resize(300, 300);
    widget->setLayout(gameCenter);
    widget->show();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::buttonPressed()
{
    // change the text to test event
    desc->setText("worked!");
}
