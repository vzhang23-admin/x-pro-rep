#include "mainwindow.h"

#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QPushButton>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QtGui>
#include <QFormLayout>

// Functions
void setupWindow(int width, int height, QWidget gameCenter);

void setupWindow(int width, int height, QWidget *gameCenter)
{
    gameCenter->setWindowTitle("X Pro");
    gameCenter->resize(width, height);
    gameCenter->setMaximumSize(width, height); // Set maximum windows size
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setStyle("fusion");
    MainWindow mainWindow;
    return app.exec();
}
