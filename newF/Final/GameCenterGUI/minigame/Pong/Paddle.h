/** 
 * @file Paddle.h
 * @brief Header file for Paddle.cpp
 * @author Bohr Deng
 * */

#ifndef PADDLE_H
#define PADDLE_H

class Paddle {
    private:
        float x;
        float y;
        float speed;
        float width;
        float height;
    public:
        Paddle(float xPos, float yPos, float dy, float w, float h);
        void update(float d);
        float get_xPos();
        float get_yPos();
        void set_yPos(float yPos);
        float get_speed();
        void set_speed(float dy);
        float get_width();
        float get_height();
};

#endif