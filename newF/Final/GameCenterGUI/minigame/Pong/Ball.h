/** 
 * @file Ball.h
 * @brief Header file for Ball.cpp
 * @author Bohr Deng
 * */

#ifndef BALL_H
#define BALL_H

class Ball {
    private:
        float x;
        float y;
        float xSpeed;    //horizontal speed
        float ySpeed;    //vertical speed
        float width;
        float height;
    public:
        Ball(float xPos, float yPos, float dx, float dy, float w, float h);
        void update(float d1, float d2);
        float get_xPos();
        void set_xPos(float xPos);
        float get_yPos();
        void set_yPos(float yPos);
        float get_xSpeed();
        void set_Xspeed(float dx);
        float get_ySpeed();
        void set_Yspeed(float dy);
        float get_width();
        float get_height();
};

#endif