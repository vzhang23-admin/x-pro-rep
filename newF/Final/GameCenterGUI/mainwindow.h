#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QVBoxLayout>
#include <QtGui>
#include <QFormLayout>
#include <QCoreApplication>
#include <QPixmap>
#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QImage>
#include <QFormLayout>
#include <QGroupBox>
#include <QScrollArea>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <QTextEdit>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/**
 * @brief The MainWindow class
 *        contains all the variables will be used and declare their types
 *        as well as the types of functions which will be used, it is declared as well
 *
 * @return nothing
 * @author Dan Lou
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void buttonPressed();
    void startGame();
private:
    QLabel *gameImg;
    char nameGame[100];
    Ui::MainWindow *ui;
    QPushButton **list;
    QWidget *widget;
    QGridLayout *gameCenter;
    QTextEdit *desc;
    QImage *input_image;
    QFormLayout *formLayout;
    QGridLayout *imageLayout;
    QGroupBox *groupBox;
    QScrollArea *scroll;
    
};
#endif // MAINWINDOW_H
