Readme
To run the game center, qt,  openGL and DevIL(also called openIL) is required.
The command to download openGL are: (in rpi desktop) 
sudo apt-get install freeglut3
sudo apt-get install freeglut3-dev
The command to download Devil are: 
sudo apt-get install libdevil-dev
The3 command to download Qt are:
sudo apt-get install build-essential
sudo apt-get install qtcreator
sudo apt-get install qt5-default
sudo apt-get install qt5-doc qtbase5-examples qtbase5-doc-html
The full tutorial for install Qt is here:
To install Qt: https://vitux.com/compiling-your-first-qt-program-in-debian-10/?fbclid=IwAR37IcDAnnjcBZnjWfCpXYn5IDgfpBO32OIrRaz_IiSd8aXv2S2sEgcb0zI
After installing those libraries, the command to run the game center is :
	./GameCenterGUI
To recompile GameCenterGUI, do the following commands:
	qmake GameCenter.pro
	make
 
To run the minigames, a executable file is required, it is provided as ‘main’
To recompile any minigames, goes to the subfolder and run RUNME.sh to compile it, if RUNME.sh is not working(maybe is the permission error), copy and paste the code inside RUNME.sh to the console. 
 
 
Play Game
Click the button for the game you want to play, it will show you the game description on the right side of screen, under the image click the start game button to start up a new window for the game
 
Add Game
To ensure the game you added can be played please ensure you've done every step correctly:
1. Add the game folder inside "minigames" folder in GameCenterGUI
2. Make sure the game folder name is unique, the name of the folder will be displayed as the name of the game
3. Need to contain a “description.txt” and “picture.jpg” for the description and picture of the game.
	The name and extension of description and picture can not be modified
4. Inside the game folder there must be an executable file called "main"
 
Delete Game
Go inside the minigame folder and delete the file for the game you want to delete. 